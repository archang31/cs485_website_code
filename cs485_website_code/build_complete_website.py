#!/usr/bin/env python

from datetime import datetime
import os
import shutil
import sys
from pylibs.copytree import copytree
from pylibs.excel import *
import build_scoreboard as sb

courseData = dict()

# Edit this data for your course
courseData['courseNumber'] = "CS485"
courseData['courseName'] = "Ethical Hacking"
courseData['courseDirector'] = "CPT Michael Kranch (ArchAng31)"
courseData['courseDirectorPhone'] = "x5560"
courseData['courseDirectorRoomNumber'] = "TH1120"
email = "michael.kranch@usma.edu"
courseData['courseDirectorEmail'] = "<a href='{e}'>{e}</a>".format(e=email)

# Edit this data for where you files are saved (if needed)
local_website_folder = "../" + courseData['courseNumber'] + "_website/"
remote_website_folder = r"\\usmasvddeecs\EECS\S&F\Web\Internal\InternalWeb\Courses" + '\\' + courseData['courseNumber']
excel_lessons_file = os.path.join(os.getcwd(), 'lessons')
excel_lessons_num_cols = 7
excel_lessons_last_row = 48
csv_lessons_files = os.path.join(os.getcwd(), 'lessons.csv')


def set_current_tab(page):
    i = base_page.find('href="' + page + '.html"') - 1
    return base_page[:i] + ' class="current"' + base_page[i:]


def build_page(name, title):
    this_base = set_current_tab(name)
    courseData['title'] = title
    with open('base_pages/base_' + name + '.html') as f:
        courseData['pageContent'] = f.read().format(**courseData)
        #if name == "scoreboard_exercises":
        #    courseData['pageContent'] = f.read().format(**courseData).replace('.data','_'+date_time_str+'.data')
    if write_local:
        with open(local_website_folder + name + '.html', 'w') as output:
            output.write(base_page.format(**courseData))
    print("[*] Building " + name)

def turnIntoList(rowArrayFromExcel, bulletize=True):
    if rowArrayFromExcel:
        newString = ""
        for item in rowArrayFromExcel.split('\n'):
            item = checkIfLink(item)
            if bulletize:
                newString += '\t<li>' + item + '</li>\n\t\t'
            else:
                newString += item + '<br>'
        return newString
    return rowArrayFromExcel


def checkIfLinkMultipleItems(rowArrayFromExcel, target="_blank"):
    itemListString = ""
    if rowArrayFromExcel:
        for item in rowArrayFromExcel.split('\n'):
            itemListString += checkIfLink(item, target) + "<br>"
        return itemListString[:-4]
    else:
        return rowArrayFromExcel


def checkIfLink(item, target="_blank"):
    if '|' in item:
        title, link = item.rsplit('|', 1)
        return '<a href="' + link.strip() + '" target="' + target + '">' + title.strip() +  '</a>'
    return item

def build_lessons():
    with open('base_pages/base_lessons_table.txt', 'rt') as f1:
        lesson_template = f1.read()
    with open('base_pages/base_lessons_table_complete_line.txt', 'rt') as f2:
        lesson_template_single = f2.read()
    courseData['lessons_table'] = ""
    # excel file must be closed
    excel_file, workbook, worksheet = openExcel(excel_lessons_file, False, False)
    headers = getRow(worksheet, 1, range(1, excel_lessons_num_cols + 1))
    rows = getAllRows(worksheet, headers, excel_lessons_last_row)
    closeExcel(excel_file, workbook)
    with open(csv_lessons_files, 'wt') as f3:
        f3.write(",".join(headers)+'\n')
        for row in rows:
            # convert LSN number to an int if it exists
            try:
                row['Lesson'] = int(row['Lesson'])

            # This is to handle modified days that have a *
            except Exception as e:
                pass
            # convert date to DD MMM if it exists
            if row['Date']:
                row['Date'] = datetime.strftime(datetime(row['Date'].year, row['Date'].month, row['Date'].day, 0, 0, 0),
                                                '%d %b')
            row['Topic'] = turnIntoList(row['Topic'], False)
            row['Reading Before Class'] = turnIntoList(row['Reading Before Class'], False)
            row['Documents Due'] = checkIfLinkMultipleItems(row['Documents Due'])
            row['Notes'] = checkIfLinkMultipleItems(row['Notes'])
            # write to .csv from .xslx. This is useful for running the script on the server later since no Word.
            csv_string = str(row['Single Line']) + ',' + str(row['Lesson']) + ',' + str(row['Date']) + ','
            csv_string += str(row['Topic']) + ',' + str(row['Reading Before Class']) + ','
            csv_string += str(row['Documents Due']) + ',' + str(row['Notes'])
            f3.write(csv_string.replace('\n', '') + '\n')
            template = (lesson_template_single if row['Single Line'] == 'Y' else lesson_template)
            courseData['lessons_table'] += template.format(**row).replace('None', "")
            # Single Line,Lesson,Date,Topic,Reading Before Class,Documents Due,Notes
    build_page('lessons', "Schedule")

def copy_website_to_cs485_website_folder(ignore_main):
    copytree('data/', local_website_folder + 'scoreboard/data', False, ignore_main, write_only_past_week, showCopy)
    shutil.copy2('variant-multi.css', local_website_folder + 'variant-multi.css')
    shutil.copy2(local_website_folder + 'home.html', local_website_folder + 'index.html')
    copytree('js/', local_website_folder + 'js/', False, ignore_main, write_only_past_week, showCopy)
    copytree('comp_files/', local_website_folder + 'comp_files/', False, ignore_main, write_only_past_week, showCopy)
    copytree('proofs/', local_website_folder + 'proofs/', False, ignore_main, write_only_past_week, showCopy)


def copy_website_to_website_network_folder():
    ignore_extra = shutil.ignore_patterns("Thumbs.db", "scoreboard")
    copytree(local_website_folder, remote_website_folder, False, ignore_extra, write_only_past_week, showCopy)


def copy_over_scheduled_scoreboard_tasks(ignore_main):
    share_folder = r"\\usmasvddeecs\eecs\Cadet\Courses\CS\CS485\AY182\Website"
    shutil.copy2('build_scoreboard.py', share_folder)
    shutil.copy2('build_scoreboard_exercises.py', share_folder)
    shutil.copy2('build_scoreboard_labs.py', share_folder)
    shutil.copy2('build_scoreboard_comp.py', share_folder)
    copytree('pylibs/', os.path.join(share_folder, 'pylibs'), False, ignore_main, write_only_past_week, showCopy)
    copytree('data/', os.path.join(share_folder, 'data'), False, ignore_main, write_only_past_week, showCopy)
    shutil.copy2('lessons.csv', share_folder)
    shutil.copy2('lab_hosts.csv', share_folder)


# ignore_main=shutil.ignore_patterns("Thumbs.db")
# copytree(local_website_folder + '/exercises',share_folder + r"\cs485_website\exercises", False, ignore_main, write_only_past_week)


courseData['lastUpdated'] = datetime.strftime(datetime.now(), '%A, %d %B %Y')
courseData['currentYear'] = datetime.now().year
with open('base_pages/base_page.html') as f:
    base_page = f.read()
write_local = True
write_network = True
write_only_past_week = True
showCopy = False


def build_main():
    print("[m] Starting Build Main")
    # set to False if you do not want to write your data
    build_page('home', "Home")
    build_page('syllabus', "Syllabus")
    build_lessons()
    build_page('resources', "Resources")
    courseData['exercises_first_blood_table'], courseData['weekly_table'] = sb.build_scoreboard_exercises()
    courseData['labs_first_blood_table'] = sb.build_scoreboard_labs()
    build_page('scoreboard_exercises', "Exercises")
    build_page('scoreboard_labs', "Labs")
    # build_page('scoreboard_competition')

    if write_network:
        print("[m] Writing Data to Websites")
        ignore_main = shutil.ignore_patterns("Thumbs.db", "*.pyc")
        copy_website_to_cs485_website_folder(ignore_main)
        copy_website_to_website_network_folder()
        copy_over_scheduled_scoreboard_tasks(ignore_main)


def build_test():
    # set to False if you do not want to write your data
    print("Building Test Config")
    build_page('home')
    build_page('scoreboard_competition')
    ignore_main = shutil.ignore_patterns("Thumbs.db")
    copy_website_to_cs485_website_folder(ignore_main)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        build_main()
    else:
        build_test()
