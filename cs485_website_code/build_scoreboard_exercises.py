#!/usr/bin/env python

from datetime import datetime, timedelta
from time import mktime
import os
import json
import numpy
import csv
import re


def get_default_values():
    sb_data = dict()
    sb_data['write_to_file'] = True
    sb_data['assignment_type'] = "Exercise"
    sb_data['assignment_folder'] = "Exercises"
    sb_data['assignment_regex'] = '>[0-9.]+<'
    sb_data['measurements_per_day'] = 4
    sb_data['points_per_assignment'] = 5.5
    sb_data['bonus_points_first_blood'] = 2
    sb_data['bonus_points_per_week'] = 5
    sb_data['negative_points_per_late'] = .5
    sb_data['max_off_per_assignment'] = 2.5
    sb_data['scare_students'] = False
    sb_data['start_date'] = datetime(2018, 1, 8)
    sb_data['end_date'] = datetime(2018, 4, 11)
    #-year-hour format
    sb_data['due_time'] = '-2018-09'
    sb_data['scoreTwoFirstBloods'] = True
    return sb_data


def build_scoreboard(sb_data, nicknames, teams):
    # The following code sets needs function variables called by sub functions
    # This code sets save location based on whether run locally or on the shared folder
    current_dir = os.getcwd()
    if current_dir == r"C:\windows\system32" or current_dir == r"E:\DFS\cadet-courses\CS\CS485\AY182\Website":
        print("[*] Building scoreboard with remote settings")
        root_dir = r"E:\DFS\cadet-courses\CS\CS485"
        local_scoreboard_data_folder = root_dir + r"\AY182\Website"
    else:
        print("[*] Building scoreboard with local settings")
        root_dir = r"\\usmasvddeecs\eecs\Cadet\Courses\CS\CS485"
        local_scoreboard_data_folder = current_dir
    local_lesson_location = local_scoreboard_data_folder + r"\lessons.csv"
    turn_in_root_dir = root_dir + r"\AY182\Students"
    # Based on # of students in the turnInFolder
    number_of_students = len(os.listdir(turn_in_root_dir))
    size_of_teams = len(teams[0])

    # Get the exercises from the lessons.csv
    assignments = []
    assignments_due = []
    assignment_due_dates = dict()
    current_date = datetime.now()
    with open(local_lesson_location, 'rt') as f:
        lesson_reader = csv.DictReader(f)
        for row in lesson_reader:
            for document in row['Documents Due'].split('<br>'):
                e = re.search(sb_data['assignment_regex'], document)
                if e:
                    if '.txt' in e.group():
                        assignment = e.group()[:-4]
                    else:
                        assignment = e.group()[1:-1]
                    due_date = datetime.strptime(row['Date']+sb_data['due_time'], '%d %b-%Y-%H')
                    if sb_data['start_date'] <= due_date <= sb_data['end_date']:
                        assignments.append(assignment)
                        # below line if I could use excel file but can not on the remote server since no office
                        # exerciseDueDates[exercise] = datetime(row['Date'].year,
                        #  row['Date'].month, row['Date'].day, 14, 0, 0)
                        assignment_due_dates[assignment] = due_date
                        # Below is a way to split the PWK exercises into two groups
                        # if exerciseDueDates[exercise] < pwk1ExerciseCutoffDate:
                        #    print(exercise, "PWK1")
                    if due_date < current_date:
                        assignments_due.append(assignment)

    assignments_complete = dict()
    number_of_assignments = len(assignments)
    max_total_points = number_of_assignments * sb_data['points_per_assignment']

    # build time bins for length of course from startdate until current time
    date_counter = sb_data['start_date']
    x_axis_date_labels = []
    # team_names = [team['name'] for team in teams]
    student_names = sorted(nicknames, key=nicknames.get)
    student_names.remove('Due Dates')
    student_nicknames = []
    for name in student_names:
        student_nicknames.append(nicknames[name])

    while date_counter < current_date and date_counter < sb_data['end_date']:
        x_axis_date_labels.append(format(date_counter, '%b %d'))
        date_counter += timedelta(hours=24 / sb_data['measurements_per_day'])
    number_of_time_bins = len(x_axis_date_labels)

    def get_time_bin(file_date):
        time_since_start = file_date - mktime(sb_data['start_date'].timetuple())
        return int(time_since_start / (3600*24 / sb_data['measurements_per_day']))

    def get_cadet_solves_data_from_share_folder():
        cdt_solves = dict()
        for cdtDir in os.listdir(turn_in_root_dir):
            cdt_solves[cdtDir] = dict()
            cdt_solves[cdtDir]['solves'] = [0]*number_of_time_bins
            cdt_solves[cdtDir]['totalBonus'] = 0
            cdt_solves[cdtDir]['firstBloods'] = 0
            cdt_solves[cdtDir]['teamBonus'] = 0
            cdt_solves[cdtDir]['latePoints'] = 0
            cdt_solves[cdtDir]['lateList'] = []
            #start with all exercises as part of the missing list
            cdt_solves[cdtDir]['missingList'] = list(assignments_due)
            assignment_directory = os.path.join(turn_in_root_dir, cdtDir, sb_data['assignment_folder'])
            late_exercises_file = open(os.path.join(turn_in_root_dir, cdtDir, 'exercisesLate.txt'), 'wt')
            late_exercises_file.write("{:<20}  {:<8}  {:<12}  {:<12}\n".format("Name", sb_data['assignment_type'],
                                                                              "Date Due", "Date Submitted"))
            missing_exercises_file = open(os.path.join(turn_in_root_dir, cdtDir, 'exercisesMissing.txt'), 'wt')
            missing_exercises_file.write("{:<20}  {:<8}  {:<12}\n".format("Name", sb_data['assignment_type'],
                                                                              "Date Due"))
            for file in sorted(os.listdir(assignment_directory)):
                exercise, extension = file.rsplit('.', 1)
                #print(exercise, extension)
                if exercise not in assignments:
                    print("Bad exercises for " + cdtDir + ' ' + file)
                elif extension == 'txt':
                    print("Documentation for " + cdtDir + ' ' + file)
                elif extension == 'txt':
                    print("Bad extension for " + cdtDir + ' ' + file)
                else:
                    if exercise not in assignments_complete:
                        assignments_complete[exercise] = dict()
                    ### remove assignment from array of already due if in that Array
                    if exercise in cdt_solves[cdtDir]['missingList']:
                        cdt_solves[cdtDir]['missingList'].remove(exercise)
                    exercise_due_date = assignment_due_dates[exercise]
                    file_turn_in_time = os.path.getmtime(os.path.join(assignment_directory, file))
                    if file_turn_in_time > mktime(sb_data['end_date'].timetuple()):
                        print(cdtDir + " " + exercise + " past due date")
                        continue
                    time_bin = get_time_bin(file_turn_in_time)
                    cdt_solves[cdtDir]['solves'][time_bin] += sb_data['points_per_assignment']
                    assignments_complete[exercise][file_turn_in_time] = cdtDir
                    # if the exercise is turned in late
                    if file_turn_in_time > mktime(exercise_due_date.timetuple()):
                        turn_in_time = datetime.strftime(datetime.fromtimestamp(file_turn_in_time), '%m-%d-%H:%M')
                        due_time = datetime.strftime(exercise_due_date, '%m-%d-%H:%M')
                        days_late = datetime.fromtimestamp(file_turn_in_time) - exercise_due_date
                        weeks_late = days_late.days/7 + 1
                        late_exercises_file.write("{:<20}  {:<8}  {:<12}  {:<12}\n".format(cdtDir, exercise, due_time, turn_in_time))
                        #the students get 20% off per week up to -60%
                        cdt_solves[cdtDir]['latePoints'] -= min(weeks_late * sb_data['negative_points_per_late'],
                            sb_data['max_off_per_assignment'])
                        cdt_solves[cdtDir]['lateList'].append(exercise)
            late_exercises_file.close()
            for missingExercise in cdt_solves[cdtDir]['missingList']:
                missing_due_time = datetime.strftime(assignment_due_dates[missingExercise], '%m-%d-%H:%M')
                missing_exercises_file.write("{:<20}  {:<8}  {:<12}\n".format(cdtDir, missingExercise, missing_due_time))
                #add negative points for a missing assignment
                if sb_data['scare_students']:
                    cdt_solves[cdtDir]['latePoints'] -= sb_data['max_off_per_assignment']
            missing_exercises_file.close()
        return cdt_solves

    def split_exercise(exercise):
        """Split a exercise address given as string into a 3 or 4-tuple of integers."""
        return tuple(int(part) for part in exercise.split('.'))

    def exercise_key(item):
        return split_exercise(item[0])

    def score_first_bloods(cdt_solves):
        first_blood_HTML_string = ''
        tr = '\t\t<tr>\n\t\t\t<td><a href="exercises/{exercise}.JPG">{exercise}</a></td>\n\t\t\t<td class="blood">{firstBlood}</td>'
        tr += '\n\t\t\t<td>{firstBloodTime}</td>\n\t\t\t<td>{lastCompTime}</td>\n\t\t\t<td>{completed}</td>\n'
        for assignment in sorted(assignments_complete.items(), key=exercise_key):
            # put all the completed times in an array
            completed = []
            assignment = assignment[0]
            for time in assignments_complete[assignment]:
                completed.append(nicknames[assignments_complete[assignment][time]])
            # sort by completed time, pick the first completed to determine first blood
            earliest_date = sorted(assignments_complete[assignment])[0]
            last_date = sorted(assignments_complete[assignment])[-1]
            # add pertinent values to dictionary to use in string format to build html
            assignments_complete[assignment]['exercise'] = str(assignment)
            timestamp = datetime.fromtimestamp(earliest_date)
            assignments_complete[assignment]['firstBloodTime'] = datetime.strftime(timestamp, '%m-%d at %H:%M')
            # we know the earliestDate so look into the exercisesComplete to find the CDT that submitted it
            cdt_dir = assignments_complete[assignment][earliest_date]
            assignments_complete[assignment]['firstBlood'] = nicknames[cdt_dir]
            if len(completed) == number_of_students:
                assignments_complete[assignment]['completed'] = "<b>All Students</b>"
                timestamp = datetime.fromtimestamp(last_date)
                assignments_complete[assignment]['lastCompTime'] = datetime.strftime(timestamp, '%m-%d at %H:%M')
            else:
                assignments_complete[assignment]['completed'] = ", ".join(sorted(completed))
                assignments_complete[assignment]['lastCompTime'] = 'Not All Complete'
            #moved doing the string to the never end incase 2 first bloods
            # add points to the cadetSolve for students that get first blood
            time_bin = get_time_bin(earliest_date)
            cdt_solves[cdt_dir]['solves'][time_bin] += sb_data['bonus_points_first_blood']
            cdt_solves[cdt_dir]['totalBonus'] += sb_data['bonus_points_first_blood']
            cdt_solves[cdt_dir]['firstBloods'] += sb_data['bonus_points_first_blood']
            if len(completed) > 1 and sb_data['scoreTwoFirstBloods']:
                second_earliest_date = sorted(assignments_complete[assignment])[1]
                cdt_dir2 = assignments_complete[assignment][second_earliest_date]
                cdt_solves[cdt_dir2]['solves'][time_bin] += sb_data['bonus_points_first_blood']
                cdt_solves[cdt_dir2]['totalBonus'] += sb_data['bonus_points_first_blood']
                cdt_solves[cdt_dir2]['firstBloods'] += sb_data['bonus_points_first_blood']
                #recreate first_blood_HTML_string
                assignments_complete[assignment]['firstBlood'] = nicknames[cdt_dir] + ', ' + nicknames[cdt_dir2]
            first_blood_HTML_string += tr.format(**assignments_complete[assignment])
        return first_blood_HTML_string

    def compute_team_solves(cdt_solves):
        tm_solves = dict()
        for team in teams:
            tm_solves[team['name']] = dict()
            tm_solves[team['name']]['solves'] = [0]*number_of_time_bins
            combined_team_solves = []
            for member in team['members']:
                # add the solves of each member to teamSolves Array
                combined_team_solves.append(cdt_solves[member]['solves'])
            # sum together and divide by # of team members
            tm_solves[team['name']]['solves'] = [sum(x)/size_of_teams for x in zip(*combined_team_solves)]
        return tm_solves

    def compute_due_dates_graph_line(cdt_solves):
        cdt_solves['Due Dates'] = dict()
        cdt_solves['Due Dates']['solves'] = [0] * number_of_time_bins
        for exercise, dueDate in assignment_due_dates.iteritems():
            if dueDate < current_date:
                time_bin = get_time_bin(mktime(dueDate.timetuple()))
                cdt_solves['Due Dates']['solves'][time_bin] += sb_data['points_per_assignment']
        return cdt_solves

    def compute_winning_team_weekly(cdt_solves, tm_solves):
        weekly_HTML_string = ''
        tr = '\t\t<tr>\n\t\t\t<td>{weekNum}</td>\n\t\t\t<td>{date}</td>\n\t\t\t<td>{team}</td>\n'
        weekly_dict = {}
        #change to first monday morning
        monday_mornings = datetime(2018, 1, 15)
        week_num = 0
        # Initialize teamScore Array
        monday_chart_labels = []
        for team in teams:
            tm_solves[team['name']]['teamScore'] = []
        while monday_mornings < current_date and monday_mornings < sb_data['end_date']:
            week_num += 1
            monday_morning = get_time_bin(mktime(monday_mornings.timetuple()))
            ### This is code to specifically skip weeks 8, 9, and 10 due to spring break / NCX
            monday_morning_label = datetime.strftime(monday_mornings, '%b %d')
            if monday_morning_label in ['Mar 05','Mar 12','Mar 19','Mar 26']:
                monday_mornings += timedelta(days=7)
                continue
            team_scores = []
            for team in teams:
                team_score = tm_solves[team['name']]['cdf'][monday_morning]
                team_scores.append(team_score)
                tm_solves[team['name']]['teamScore'].append(
                    round((float(team_score)*100 / sb_data['points_per_assignment']) / number_of_assignments, 2))
            weekly_dict['team'] = []
            for team in teams:
                if tm_solves[team['name']]['cdf'][monday_morning] == max(team_scores):
                    weekly_dict['team'].append(team['name'])
            for team in teams:

                if team['name'] in weekly_dict['team']:
                    for member in team['members']:
                        cdt_solves[member]['solves'][monday_morning] += sb_data['bonus_points_per_week']
                        cdt_solves[member]['totalBonus'] += sb_data['bonus_points_per_week']
                        cdt_solves[member]['teamBonus'] += sb_data['bonus_points_per_week']
            weekly_dict['date'] = datetime.strftime(monday_mornings, '%A, %B %d at %H:%M')
            monday_chart_labels.append(monday_morning_label)
            weekly_dict['weekNum'] = str(week_num)
            weekly_dict['team'] = ", ".join(weekly_dict['team'])
            weekly_HTML_string += tr.format(**weekly_dict)
            monday_mornings += timedelta(days=7)

        return weekly_HTML_string, cdt_solves, tm_solves, monday_chart_labels

    def compute_cdf(solves):
        for cdt_dir in solves:
            solve_str = str(numpy.cumsum(solves[cdt_dir]['solves']))[1:-1]
            solves[cdt_dir]['cdf'] = map(float, [s.strip() for s in solve_str.split(' ') if s])
            solves[cdt_dir]['totalExercisePoints'] = solves[cdt_dir]['cdf'][-1]
            solves[cdt_dir]['percentageComplete'] = [round((float(x) / max_total_points)*100, 1) for x in solves[cdt_dir]['cdf']]
        return solves

    def generate_exercises_chart_data(solves_data):
        chart_data_array = []
        sorted_solves_names = sorted(nicknames, key=nicknames.get)
        for team in sorted(teams):
            sorted_solves_names.append(team['name'])
        for solves_data_name in sorted_solves_names:
            chart_data = dict()
            if solves_data_name in nicknames:
                chart_data['name'] = nicknames[solves_data_name]
            else:
                chart_data['name'] = solves_data_name
            chart_data['data'] = solves_data[solves_data_name]['percentageComplete']
            # Set data to visible by default
            chart_data['visible'] = True
            # Handle Due Dates Line
            if solves_data_name == "Due Dates":
                chart_data['type'] = "area"
                chart_data['color'] = "rgba(255,0,0,1)"
                chart_data['step'] = "left"
                # Save Due Date Data to add last
                due_date_data = chart_data
            else:
                chart_data['type'] = "line"
                for team in teams:
                    if solves_data_name == team['name']:
                        chart_data['color'] = team['color']
                        chart_data['step'] = "left"
                        chart_data['visible'] = False
                        chart_data['dashStyle'] = "longdash"
                    elif "TM" not in solves_data_name:
                        for member in team['members']:
                            # this is because this could be a team or just a CDT
                            if solves_data_name == member:
                                chart_data['color'] = team['color']
                                chart_data['step'] = "left"
                                chart_data['dashStyle'] = "solid"
                chart_data_array.append(chart_data)
        # finally, add the dueDateData at the end so its line is the top line
        chart_data_array.append(due_date_data)
        return chart_data_array

    def generate_teams_chart_data(solves_data):
        chart_data_array = []
        for team_dir in sorted(solves_data):
            chart_data = dict()
            chart_data['name'] = team_dir
            for team in teams:
                if team_dir == team['name']:
                    chart_data['type'] = "column"
                    chart_data['color'] = team['color']
                    chart_data['data'] = solves_data[team_dir]['teamScore']
            chart_data_array.append(chart_data)
        return chart_data_array

    def generate_points_chart_data(cdt_solves):
        points_array = []
        # {name: 'John', data: [5, 3, 4, 7, 2]}
        first_blood_bonus = dict()
        first_blood_bonus['name'] = "First Blood Bonus"
        first_blood_bonus['color'] = "orange"
        first_blood_bonus['data'] = []
        for student in student_names:
            first_blood_bonus['data'].append(cdt_solves[student]['firstBloods'])
        points_array.append(first_blood_bonus)
        week_bonus = dict()
        week_bonus['name'] = "Weekly Team Bonus"
        week_bonus['color'] = "blue"
        week_bonus['data'] = []
        for student in student_names:
            week_bonus['data'].append(cdt_solves[student]['teamBonus'])
        points_array.append(week_bonus)
        exercise_points = dict()
        exercise_points['name'] = "Exercise Completion Points"
        exercise_points['color'] = "green"
        exercise_points['data'] = []
        for student in student_names:
            exercise_points['data'].append(cdt_solves[student]['totalExercisePoints'])
        points_array.append(exercise_points)
        negative_late_points = dict()
        negative_late_points['name'] = "Lost Late Points"
        negative_late_points['color'] = "red"
        negative_late_points['data'] = []
        for student in student_names:
            negative_late_points['data'].append(cdt_solves[student]['latePoints'])
        points_array.append(negative_late_points)
        with open('grades_exercises.csv', 'w') as grade_file:
            grade_file.write("Student Name, Execise Points, Team Bonus, First Bloods, Late Points\n")
            for student in sorted(student_names):
                grade_string = str(student) + ', '
                grade_string += str(cdt_solves[student]['totalExercisePoints']) + ', '
                grade_string += str(cdt_solves[student]['teamBonus']) + ', '
                grade_string += str(cdt_solves[student]['firstBloods']) + ', '
                grade_string += str(cdt_solves[student]['latePoints']) + '\n'
                grade_file.write(grade_string)
        return points_array

    def write_chart_data_to_file(chart_data, x_axis_labels, file_name, function_name):
        output_string = function_name + '([\n'
        output_string += json.dumps(x_axis_labels, sort_keys=True, indent=4, separators=(',', ': '))
        output_string += ',\n'
        output_string += json.dumps(chart_data, sort_keys=True, indent=4, separators=(',', ': '))
        output_string += '\n])'
        with open(os.path.join(local_scoreboard_data_folder, 'data', file_name), 'w') as localData:
            localData.write(output_string)

    cadet_solves = get_cadet_solves_data_from_share_folder()
    # cadetSolves = compute__cdf(cadetSolves)
    # recompute the cadet CDF with team points and bonus points
    cadet_solves = compute_due_dates_graph_line(cadet_solves)
    cadet_solves = compute_cdf(cadet_solves)
    team_solves = compute_team_solves(cadet_solves)
    team_solves = compute_cdf(team_solves)
    # update cadetSolves with the teamSolves. We do this so team will be visible in the exerciseChart
    cadet_solves.update(team_solves)
    exercise_chart_data = generate_exercises_chart_data(cadet_solves)
    # calculate the winning team and firstBlood after generate exercises.js so the bonus values are not included in the charts.
    weekly_HTML, cadet_solves, team_solves, monday_chart_labels = compute_winning_team_weekly(cadet_solves, team_solves)
    teams_chart_data = generate_teams_chart_data(team_solves)
    first_blood_HTML = score_first_bloods(cadet_solves)
    points_chart_data = generate_points_chart_data(cadet_solves)
    if sb_data['write_to_file']:
        #date_time_str = datetime.strftime(current_date, '%y%m%d%H%M%S')
        write_chart_data_to_file(exercise_chart_data, x_axis_date_labels, "exercises.js", 'create_exercises_chart')
        write_chart_data_to_file(teams_chart_data, monday_chart_labels,  "exercises_teams.js", 'create_exercises_teams_chart')
        write_chart_data_to_file(points_chart_data, student_nicknames,  "exercises_points.js", 'create_exercises_points_chart')
    print("The number of exercises is: " + str(number_of_assignments))
    return first_blood_HTML, weekly_HTML

if __name__ == "__main__":
    scoreboard_data = get_default_values()
    build_scoreboard(scoreboard_data)
