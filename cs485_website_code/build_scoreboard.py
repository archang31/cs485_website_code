#!/usr/bin/env python

import build_scoreboard_exercises as esb
import build_scoreboard_labs as lsb


def get_nicknames():
    nicknames = dict()
    nicknames['ADAM_MASON'] = "Adambomb"
    nicknames['BRUNNER_RYAN'] = "Vimgod"
    nicknames['CHAUSSEY_BRENNEN'] = "Gowther"
    nicknames['ECKERT_CONNOR'] = "Atlas"
    nicknames['FAUERBACH_KYLE'] = "Fauer4effect"
    nicknames['GLAZER_GABRIEL'] = "GlazedDonut"
    nicknames['MILLER_SAMUEL'] = "IronGiant"
    nicknames['MULLEN_CONNOR'] = "Mullenator"
    nicknames['PRITCHARD_PRESTON'] = "Stonepresto"
    nicknames['RODRIGUEZ_NICOLAS'] = "R0d"
    nicknames['SCHLESSINGER_JOSEPH'] = "Karamazov"
    nicknames['SCHNEIDER_MADELEINE'] = "BlueberryNinja"
    nicknames['Due Dates'] = "Due Dates"
    return nicknames


def get_teams():
    teams = [
        {'name': "TM BlueStoneRod", 'members': ['PRITCHARD_PRESTON', 'SCHNEIDER_MADELEINE', 'RODRIGUEZ_NICOLAS'], 'color': 'purple'},
        {'name': "TM GlazedEffect", 'members': ['FAUERBACH_KYLE', 'GLAZER_GABRIEL','BRUNNER_RYAN'], 'color': 'green'},
        {'name': "TM IronAtlas", 'members': ['ECKERT_CONNOR', 'MILLER_SAMUEL', 'CHAUSSEY_BRENNEN'], 'color': 'orange'},
        {'name': "TM NoNames", 'members': ['ADAM_MASON', 'SCHLESSINGER_JOSEPH','MULLEN_CONNOR'], 'color': 'blue'}]


    return teams


def build_scoreboard_exercises():
    scoreboard_data = esb.get_default_values()
    nicknames = get_nicknames()
    teams = get_teams()
    return esb.build_scoreboard(scoreboard_data, nicknames, teams)


def build_scoreboard_labs():
    scoreboard_data = lsb.get_default_values()
    nicknames = get_nicknames()
    teams = get_teams()
    return lsb.build_scoreboard(scoreboard_data, nicknames, teams)


if __name__ == "__main__":
    build_scoreboard_exercises()
    build_scoreboard_labs()
