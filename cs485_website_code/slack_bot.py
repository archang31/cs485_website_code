#!/usr/bin/env python

import os
import time
from slackclient import SlackClient

BOT_ID = 'U42APT8DV'
AT_BOT = "<@" + BOT_ID + ">"
EXAMPLE_COMMAND = "!"


def get_slack_bot_id(sc):
    BOT_NAME = "cs485_bot"
    api_call = sc.api_call("users.list")
    if api_call.get('ok'):
        # retrieve all users so we can find our bot
        users = api_call.get('members')
        for user in users:
            if 'name' in user and user.get('name') == BOT_NAME:
                print("Bot ID for '" + user['name'] + "' is " + user.get('id'))
    else:
        print("could not find bot user with the name " + BOT_NAME)


def handle_command(command, channel):
    """
        Receives commands directed at the bot and determines if they
        are valid commands. If so, then acts on the commands. If not,
        returns back what it needs for clarification.
    """
    response = "Not sure what you mean. Use the *" + EXAMPLE_COMMAND + \
               "* command with numbers, delimited by spaces."
    if command.startswith(EXAMPLE_COMMAND):
        response = "Sure...write some more code then I can do that!"
    slack_client.api_call("chat.postMessage", channel=channel,
                          text=response, as_user=True)


def send_slack_message(msg):
    slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        text=msg,
        as_user=True
    )


def parse_slack_output(slack_rtm_output):
    """
        The Slack Real Time Messaging API is an events firehose.
        this parsing function returns None unless a message is
        directed at the Bot, based on its ID.
    """
    output_list = slack_rtm_output
    if output_list and len(output_list) > 0:
        for output in output_list:
            if output['type'] == "message" and 'user' in output:
                if output['user'] not in users:
                    users[output['user']] = slack_client.api_call("users.info", user=output['user'])['user']
                print(users[output['user']]['name'] +": " + output['text'])
                print(output)
    return None, None


def get_slack_channel_history(chan):
    READ_WEBSOCKET_DELAY = 1  # 1 second delay between reading from firehose
    response = slack_client.api_call("groups.history", channel=chan)
    print(response['ok'])
    for key, value in response.items():
        print(key)

    if slack_client.rtm_connect():
        print("CS485 Scorebot connected and running!")
        while True:
            command, channel = parse_slack_output(slack_client.rtm_read())
            print(command, channel)
            if command and channel:
                handle_command(command, channel)
            time.sleep(READ_WEBSOCKET_DELAY)
    else:
        print("Connection failed. Invalid Slack token or bot ID?")


def get_channels(sc):
    for channel in sc.api_call("channels.list")['channels']:
        if 'cs485' in channel['name']:
            print(channel)


def run_basic_bot():
    READ_WEBSOCKET_DELAY = 1 # 1 second delay between reading from firehose
    if slack_client.rtm_connect():
        print("StarterBot connected and running!")
        while True:
            command, channel = parse_slack_output(slack_client.rtm_read())
            if command and channel:
                print(channel)
                print(command)
                handle_command(command, channel)
            time.sleep(READ_WEBSOCKET_DELAY)
    else:
        print("Connection failed. Invalid Slack token or bot ID?")


if __name__ == "__main__":
    BOT_NAME = 'cs485_bot'
    slackToken = "removed_and_no_longer_valid"
    slack_client = SlackClient(slackToken)
    # bot_id = get_slack_bot_id(slack_client)
    test_channel = "C593E7K8W"  # test channel
    # channel = "C573GEHKJ"  # real channel
    users = {}
    run_basic_bot()
