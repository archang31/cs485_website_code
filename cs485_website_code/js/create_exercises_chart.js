function create_exercises_chart(data) {
	let xAxisData = data[0];
	let seriesData = data[1];
	console.log(xAxisData);
	console.log(seriesData);
	var ctx = document.getElementById("exercises_chart");
	Highcharts.chart(ctx, {
	    title: {
	        text: 'Exercise Completion Percentage',
	        align: "center",
	        style: {
	        	fontWeight: 'bold',
	        	fontSize: "20px"
	        }
	    },
	    xAxis: {
	        categories: xAxisData
	    },
	   	plotOptions: {
	        series: {
	            fillOpacity: 0.2
	        },
	        line: { /* or spline, area, series, areaspline etc.*/
		        marker: {
		           enabled: false
		        }
		    },
		    area: { /* or spline, area, series, areaspline etc.*/
		        marker: {
		           enabled: false
		        }
		    }
	    },
	    yAxis: {
	        title: {
	            text: 'Percentage Complete'
	        },
	        plotLines: [{
	            value: 0,
	            width: 1,
	            color: '#808080'
	        }],
	        min: 0,
	        max: 100
	    },
	    tooltip: {
	        valueSuffix: '%'
	    },
	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'middle',
	        borderWidth: 0
	    },
	    series: seriesData
	});
};