#!/usr/bin/env python

import csv
import json
import os
import build_scoreboard as sb
from datetime import datetime, timedelta
from time import mktime
import numpy
import time
import sys

comp_folder = "../archang31.pw/"


# adjust for time zones.
def get_time():
    return datetime.now() - timedelta(hours=4)


def get_default_values():
    sb_data = dict()
    sb_data['write_to_file'] = True
    # three measurement per minute
    sb_data['measurements_per_day'] = 24 * 60 * 3
    sb_data['assignments_per_lab_machine'] = 2
    sb_data['base_points'] = 20
    sb_data['first_blood_points'] = 10
    sb_data['start_date'] = datetime(2017, 5, 4, 16, 00)
    sb_data['end_date'] = datetime(2017, 5, 4, 17, 31)
    sb_data['first_blood_color'] = 'red'
    sb_data['base_score_color'] = 'blue'
    sb_data['no_score_color'] = '#DDDDDD'
    sb_data['comp_folder_location'] = "../archang31.pw/comp/"
    return sb_data


def get_nicknames():
    nick_names = sb.get_nicknames()
    nick_names.pop('Due Dates')
    return nick_names


def get_counters(nicks):
    counts = dict()
    student_counter = 1
    for student in sorted(nicks, key=nicks.get):
        counts[nicks[student]] = student_counter
        student_counter += 1
    return counts


def get_assignments(start_fresh=False):
    if start_fresh:
        local_hosts_location = os.path.join(os.getcwd(), 'lab_hosts.csv')
        # Get the exercises from the lessons.csv
        hosts_dict = dict()
        with open(local_hosts_location)as f2:
            host_reader = csv.DictReader(f2)
            for row in host_reader:
                if row['Proof'] != '' and row['hostname'][-1] != '2':
                    host = dict()
                    host['IP'] = row['IP']
                    host['hostname'] = row['hostname']
                    host['proof'] = row['Proof'].strip()
                    if row['Public']:
                        host['network'] = 'Public'
                    elif row['IT']:
                        host['network'] = 'IT'
                    elif row['Dev']:
                        host['network'] = 'Dev'
                    else:
                        host['network'] = 'Admin'
                    host['solved'] = False
                    host['solvedTime'] = "Not Yet Solved"
                    host['firstBlood'] = False
                    host['earned'] = 0
                    hosts_dict[row['IP']] = host
        save_student_data('hosts', hosts_dict)
    else:
        hosts_dict = load_student_data('hosts')
    proofs_dict = dict()
    for host in hosts_dict:
        proofs_dict[hosts_dict[host]['proof']] = hosts_dict[host]['IP']
    return hosts_dict, proofs_dict


def save_student_data(student_name, solves):
    with open(comp_folder + 'comp_files/comp_' + student_name.lower() + '.save', 'wt') as f:
        f.write(json.dumps(solves, sort_keys=True, indent=4, separators=(',', ': ')))


def generate_initial_student_saves(nicks, default_assignments):
    # add bonus to each student save
    host = dict()
    host['hostname'] = 'Bonus'
    host['solved'] = False
    host['solvedTime'] = "None Assigned Yet"
    host['firstBlood'] = False
    host['earned'] = 0
    default_assignments['Bonus'] = host
    for student in nicks:
        save_student_data(nicks[student], default_assignments)


def load_student_data(student_name):
    with open(comp_folder + 'comp_files/comp_' + student_name.lower() + '.save', 'rt') as f:
        return json.load(f)


def load_all_students_data(nicks):
    solves = {}
    for student in nicks:
        solves[nicks[student]] = load_student_data(nicks[student])
    return solves


def save_pie_chart_data(student, student_counter, solves, sb_data):
    output_string = 'create_comp_points_chart([\n'
    output_string += "\t'" + student + "',\n"
    output_string += "\t'comp_pie_chart_student" + str(student_counter) + "',\n\t[\n"
    for machine in solves[student]:
        output_string += '\t\t{\n'
        output_string += "\t\t\tname: '" + solves[student][machine]['hostname'].title() + "',\n"
        output_string += "\t\t\tearned: " + str(solves[student][machine]['earned']) + ",\n"
        output_string += "\t\t\ty: " + str(sb_data['base_points']) + ",\n"
        if solves[student][machine]['firstBlood']:
            color = sb_data['first_blood_color']
            solved_time = solves[student][machine]['solvedTime'].split(' at ', 1)[1]
        elif solves[student][machine]['earned'] != 0:
            color = sb_data['base_score_color']
            solved_time = solves[student][machine]['solvedTime'].split(' at ', 1)[1]
        else:
            color = sb_data['no_score_color']
            solved_time = solves[student][machine]['solvedTime']
        output_string += "\t\t\tcolor: '" + color + "',\n"
        output_string += "\t\t\tsolvedTime: '" + solved_time + "',\n"
        output_string += '\t\t},\n'
    output_string += '\t]\n'
    output_string += '])\n'
    with open(comp_folder + 'scoreboard/data/comp_student' + str(student_counter) + '.data', 'wt') as f:
        f.write(output_string)


def do_initial_setup(sb_data, default_assignments, nicks):
    generate_initial_student_saves(nicknames, default_assignments)
    solves = load_all_students_data(nicknames)
    for student in sorted(nicks, key=nicks.get):
        save_pie_chart_data(nicks[student], counters[nicks[student]], solves, sb_data)
    return solves


def generate_scoreboard_data(sb_data, saved_data):
    def create_chart_solves_from_saved_data():
        solves_dict = dict()
        for student in student_solves:
            solves_dict[student] = dict()
            solves_dict[student]['solves'] = [0] * number_of_time_bins
            for machine in saved_data[student]:
                if saved_data[student][machine]['earned'] != 0:
                    solved_time = datetime.strptime('2017-'+saved_data[student][machine]['solvedTime'],
                                                    '%Y-%m-%d at %H:%M:%S')
                    time_bin = get_time_bin(mktime(solved_time.timetuple()))
                    try:
                        solves_dict[student]['solves'][time_bin] += saved_data[student][machine]['earned']
                    except:
                        print("ERROR: ****************************************")
                        print("length of solves dict " + str(len(solves_dict[student]['solves'])))
                        print("timebin: " + str(time_bin))
                        print(student, machine, solved_time)
        return solves_dict

    def compute_cdf(solves):
        for student in solves:
            # you need to map the output of cdf to int because its natively a numpy.int64 which json does not know
            # how to serialize
            solves[student]['cdf'] = map(int, numpy.cumsum(solves[student]['solves']))
            solves[student]['totalExercisePoints'] = solves[student]['cdf'][-1]
        return solves

    def get_time_bin(file_date):
        time_since_start = file_date - mktime(sb_data['start_date'].timetuple())
        time_bin = int(time_since_start / (3600 * 24 / sb_data['measurements_per_day']))
        if time_bin < 0:
            return 0
        return time_bin-1

    def generate_chart_data(solves_data):
        chart_data_array = []
        for solves_data_name in sorted(solves_data):
            new_chart_data = dict()
            new_chart_data['name'] = solves_data_name
            new_chart_data['data'] = solves_data[solves_data_name]['cdf']
            # Set data to visible by default
            new_chart_data['visible'] = True
            new_chart_data['type'] = "line"
            chart_data_array.append(new_chart_data)
        return chart_data_array

    def write_chart_data_to_file(local_chart_data, x_axis_labels, file_name, function_name):
        output_string = function_name + '([\n'
        output_string += json.dumps(x_axis_labels, sort_keys=True, indent=4, separators=(',', ': '))
        output_string += ',\n'
        output_string += json.dumps(local_chart_data, sort_keys=True, indent=4, separators=(',', ': '))
        output_string += '\n])'
        with open(os.path.join(comp_folder + 'scoreboard/data/', file_name), 'w') as localData:
            localData.write(output_string)

    date_counter = sb_data['start_date']
    current_date = get_time()
    # twenty time test bins
    # current_date = sb_data['start_date'] + timedelta(seconds=24 * 3600 / sb_data['measurements_per_day'] * 61)
    number_of_time_bins = 0
    x_axis_date_labels = []
    while date_counter < sb_data['end_date']:
        x_axis_date_labels.append(format(date_counter, '%H:%M'))
        date_counter += timedelta(seconds=24 * 3600 / sb_data['measurements_per_day'])
        if date_counter <= current_date:
            number_of_time_bins += 1
    chart_solves = create_chart_solves_from_saved_data()
    chart_solves = compute_cdf(chart_solves)
    chart_data = generate_chart_data(chart_solves)
    write_chart_data_to_file(chart_data, x_axis_date_labels, "comp.data", "create_comp_chart")

if __name__ == "__main__":
    scoreboard_data = get_default_values()
    # remove below once done testing
    # scoreboard_data['start_date'] = get_time() - timedelta(minutes=2)
    # scoreboard_data['end_date'] = scoreboard_data['start_date'] + timedelta(minutes=30)
    nicknames = get_nicknames()
    counters = get_counters(nicknames)
    if len(sys.argv) == 2:
        reset_data = 'n'
        update_scoreboard = 'Y'
    else:
        reset_data = raw_input("Do you want to reset the data Y or n? [n]")
        update_scoreboard = raw_input("Do you want to continually update the scoreboard Y or n? [n]")
    if reset_data == 'Y':
        default_assignments, proofs = get_assignments(True)
        student_solves = do_initial_setup(scoreboard_data, default_assignments, nicknames)
    else:
        default_assignments, proofs = get_assignments()
        student_solves = load_all_students_data(nicknames)
    generate_scoreboard_data(scoreboard_data, student_solves)
    if update_scoreboard == 'Y':
        while True:
            print(datetime.strftime(get_time(), '%m-%d at %H:%M:%S'))
            time.sleep(20)
            student_solves = load_all_students_data(nicknames)
            generate_scoreboard_data(scoreboard_data, student_solves)
