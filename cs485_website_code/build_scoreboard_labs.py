#!/usr/bin/env python

from datetime import datetime, timedelta
from time import mktime
import os
import json
import numpy
import csv
import re
import operator
import math
import build_scoreboard as sb


def get_default_values():
    sb_data = dict()
    sb_data['write_to_file'] = True
    sb_data['assignment_type'] = "Lab"
    sb_data['assignment_folder'] = "Labs"
    sb_data['assignment__due_regex'] = '[0-3][0-9]{2}a?\.txt'
    sb_data['assignment_turnin_regex'] = '[0-3][0-9]{2}a?\.[tp][xd][tf]'
    sb_data['measurements_per_day'] = 4
    sb_data['required_hosts_compromised'] = 17
    sb_data['assignments_per_lab_machine'] = 2
    sb_data['points_per_assignment'] = 8
    sb_data['max_assignment_points'] = sb_data['required_hosts_compromised'] *\
                                       sb_data['assignments_per_lab_machine'] *\
                                       sb_data['points_per_assignment']
    sb_data['bonus_points_first_blood'] = 2
    #sb_data['bonus_points_per_week'] = 15
    sb_data['bonus_points_per_extra_assignment'] = 5
    sb_data['late_fixed_penalty'] = 1.5
    sb_data['late_penalty_per_period'] = .5
    sb_data['late_penalty_period'] = 1  # in days
    sb_data['late_penalty_cap'] = sb_data['points_per_assignment'] * .4  # 40% off

    sb_data['start_date'] = datetime(2018, 1, 22)
    sb_data['end_date'] = datetime(2018, 5, 15, 16)
    # determine optional assignments on the fly
    sb_data['number_of_optional_assignments'] = 0
    sb_data['scoreTwoFirstBloods'] = True
    return sb_data


def build_scoreboard(sb_data, nicknames, teams):
    # The following code sets needs function variables called by sub functions
    # This code sets save location based on whether run locally or on the shared folder
    current_dir = os.getcwd()
    if current_dir == r"C:\windows\system32" or current_dir == r"E:\DFS\cadet-courses\CS\CS485\AY182\Website":
        print("[*] Building scoreboard with remote settings")
        root_dir = r"E:\DFS\cadet-courses\CS\CS485"
        local_scoreboard_data_folder = root_dir + r"\AY182\Website"
    else:
        print("[*] Building scoreboard with local settings")
        root_dir = r"\\usmasvddeecs\eecs\Cadet\Courses\CS\CS485"
        local_scoreboard_data_folder = current_dir
    local_lesson_location = local_scoreboard_data_folder + r"\lessons.csv"
    local_hosts_location = local_scoreboard_data_folder + r"\lab_hosts.csv"
    turn_in_root_dir = root_dir + r"\AY182\Students"

    # Based on # of students in the turnInFolder
    number_of_students = len(os.listdir(turn_in_root_dir))

    # build time bins for length of course from startdate until current time

    date_counter = sb_data['start_date']
    current_date = datetime.now()
    x_axis_date_labels = []
    # team_names = [team['name'] for team in teams]
    student_names = sorted(nicknames, key=nicknames.get)
    student_names.remove('Due Dates')
    student_nicknames = []
    for name in student_names:
        student_nicknames.append(nicknames[name])
    while date_counter < current_date and date_counter < sb_data['end_date']:
        x_axis_date_labels.append(format(date_counter, '%b %d'))
        date_counter += timedelta(hours=24 / sb_data['measurements_per_day'])
    number_of_time_bins = len(x_axis_date_labels)

    def get_assignments():
        # Get the exercises from the lessons.csv
        hosts_dict = dict()
        due_dates = dict()
        with open(local_hosts_location, 'rt') as f2:
            host_reader = csv.DictReader(f2)
            for row in host_reader:
                host = dict()
                # these are machines that have duplicate last octets
                if 'a' in row['IP']:
                    host['IP'] = row['IP'][:-1]
                else:
                    host['IP'] = row['IP']
                host['hostname'] = row['hostname']
                host['proof'] = row['Proof']
                if row['Public']:
                    host['network'] = 'Public'
                elif row['IT']:
                    host['network'] = 'IT'
                elif row['Dev']:
                    host['network'] = 'Dev'
                else:
                    host['network'] = 'Admin'
                hosts_dict[row['IP']] = host
                if row['Mpwn'] != '':
                    host['solved'] = 'Yes'
                else:
                    host['solved'] = '<b>No</b>'
        with open(local_lesson_location, 'rt') as f:
            lesson_reader = csv.DictReader(f)
            for row in lesson_reader:
                for document in row['Documents Due'].split('<br>'):
                    assignment = None
                    if row['Date'] != 'None':
                        due_date = datetime.strptime(row['Date'] + '-2018-14', '%d %b-%Y-%H')
                    e = re.search(sb_data['assignment__due_regex'], document)
                    if e:
                        assignment = e.group()[:-4]
                    #option for extra labs
                    elif document == "1 of your own choice":
                        sb_data['number_of_optional_assignments'] += 1
                        assignment = str(sb_data['number_of_optional_assignments'])
                        # hosts_dict[assignment] = dict()
                    if assignment:
                        if sb_data['start_date'] <= due_date <= sb_data['end_date']:
                            if assignment in hosts_dict:
                                hosts_dict[assignment]['due_date'] = due_date
                            due_dates[assignment] = due_date
        return hosts_dict, due_dates

    def get_time_bin(file_date):
        time_since_start = file_date - mktime(sb_data['start_date'].timetuple())
        time_bin = int(time_since_start / (3600*24 / sb_data['measurements_per_day']))
        if time_bin < 0:
            return 0
        return time_bin

    def get_cadet_solves_data_from_share_folder():
        cdt_solves = dict()
        for cdt_dir in os.listdir(turn_in_root_dir):
            cdt_solves[cdt_dir] = dict()
            cdt_solves[cdt_dir]['solves'] = [0]*number_of_time_bins
            cdt_solves[cdt_dir]['totalBonus'] = 0
            cdt_solves[cdt_dir]['firstBloods'] = 0
            cdt_solves[cdt_dir]['teamBonus'] = 0
            cdt_solves[cdt_dir]['latePoints'] = 0
            cdt_solves[cdt_dir]['lateList'] = []
            cdt_solves[cdt_dir]['extra_solves'] = 0
            cdt_solves[cdt_dir]['assignments_complete'] = []
            cdt_solves[cdt_dir]['assignments_complete_times'] = dict()
            cdt_solves[cdt_dir]['optional_assignments'] = []
            assignment_directory = os.path.join(turn_in_root_dir, cdt_dir, sb_data['assignment_folder'])
            late_exercises_file = open(os.path.join(turn_in_root_dir, cdt_dir, 'labsLate.txt'), 'wt')
            late_exercises_file.write("{:<20}{:<10}{:<18}{:<13}{:<16}{:<12}{:<10}\n".format(
                "Name", "Document", "Assignment", "Date Due", "Date Submitted", "Days Late", "Penalty"))
            optional_assignment = dict()
            for assignment_file in sorted(os.listdir(assignment_directory)):
                e = re.search(sb_data['assignment_turnin_regex'], assignment_file)
                if e:
                    # extension = assignment_file.rsplit('.', 1)[1]
                    assignment_file.rsplit('.', 1)[0]
                    assignment = assignment_file.rsplit('.', 1)[0]
                    file_turn_in_time = os.path.getmtime(os.path.join(assignment_directory, assignment_file))
                    time_bin = get_time_bin(file_turn_in_time)
                    if assignment not in hosts:
                        print("Bad exercises for " + cdt_dir + ' ' + assignment_file)
                    else:
                        cdt_solves[cdt_dir]['assignments_complete'].append(assignment_file)
                        cdt_solves[cdt_dir]['assignments_complete_times'][assignment_file] = file_turn_in_time
                        if assignment not in assignments_complete:
                            assignments_complete[assignment] = dict()
                        assignments_complete[assignment][file_turn_in_time] = cdt_dir
                        cdt_solves[cdt_dir]['solves'][time_bin] += sb_data['points_per_assignment']
                        # maybe handle DEV vs IT vs ADMIN network here
                        #if hosts[assignment]['network'] == 'DEV':
                        #    cdt_solves[cdt_dir]['solves'][time_bin] += sb_data['dev_bonus_points']
                        # this is a required assignment
                        if assignment in assignment_due_dates:
                            exercise_due_date = assignment_due_dates[assignment]
                            # if the exercise is turned in late
                            if file_turn_in_time > mktime(exercise_due_date.timetuple()):
                                handle_late(file_turn_in_time, exercise_due_date, cdt_solves, late_exercises_file,
                                            cdt_dir, assignment, assignment_file)
                        else:
                            cdt_solves[cdt_dir]['optional_assignments'].append(assignment)
                            if assignment not in optional_assignment \
                                    or file_turn_in_time < optional_assignment[assignment]:
                                optional_assignment[assignment] = file_turn_in_time
                # else:
                #    print("Not a match for assignment: " + assignment_file)

            # output status of all specific assignments
            labs_status_file = open(os.path.join(turn_in_root_dir, cdt_dir, 'labsStatus.txt'), 'wt')
            labs_status_file.write("{:<18}{:<13}{:<13}{:<12}\n".format("Assignment", "Date Due", "Turned in", "Status"))
            labs_status_file.write("----------------------------------------------\n")
            for assignment in sorted(assignment_due_dates, key=assignment_due_dates.get):
                if len(assignment) == 3:
                    write_assignment_status(assignment, cdt_solves, cdt_dir,
                                            labs_status_file, assignment_due_dates[assignment])



            # determine how many optional assignments turned in and how many late
            # start by comparing each optional assignment with the date optional assignment due date
            # so if only one turned in, will need to be by lesson 40. 2 turned in lesson 39 and 40, etc.0

            opt_assigns_submitted = len(optional_assignment)
            num_opt_assigns = sb_data['number_of_optional_assignments']
            #print(sorted(optional_assignment.items(), key=operator.itemgetter(1)))
            for optional_number in reversed(range(1, num_opt_assigns+1)):
                index = num_opt_assigns - optional_number
                assignment = "Your Choice #" + str(index+1)
                #assignment = "Your Choice #" + str(optional_number)
                labs_status_file.write(assignment + '\n')
                #exercise_due_date = assignment_due_dates[str(optional_number)]
                exercise_due_date = assignment_due_dates[str(index+1)]
                variable_assignment_num = num_opt_assigns - opt_assigns_submitted
                #print(variable_assignment_num, index, index - variable_assignment_num)
                if index >= (variable_assignment_num):
                #if opt_assigns_submitted > index:
                    assignment_file, file_turn_in_time = sorted(optional_assignment.items(), key=operator.itemgetter(1))[abs(variable_assignment_num-index)]
                    for assign in [assignment_file + ".txt", assignment_file + ".pdf"]:
                        if assign in cdt_solves[cdt_dir]['assignments_complete']:
                            turn_in_time = cdt_solves[cdt_dir]['assignments_complete_times'][assign]
                            if turn_in_time > mktime(exercise_due_date.timetuple()):
                                handle_late(turn_in_time, exercise_due_date, cdt_solves, late_exercises_file,
                                            cdt_dir, assignment, assign)
                    #if file_turn_in_time > mktime(exercise_due_date.timetuple()):
                    #    handle_late(file_turn_in_time, exercise_due_date, cdt_solves, late_exercises_file,
                    #                cdt_dir, assignment, assignment_file)
                    write_assignment_status(assignment_file, cdt_solves, cdt_dir,
                                            labs_status_file, exercise_due_date)
                else:
                    write_assignment_status("###", cdt_solves, cdt_dir,
                                            labs_status_file, exercise_due_date)
            labs_status_file.close()
            late_exercises_file.close()
        return cdt_solves

    def handle_late(file_turn_in_time, exercise_due_date, cdt_solves, late_exercises_file,
                    cdt_dir, assignment, assignment_file):
            turn_in_time = datetime.strftime(datetime.fromtimestamp(file_turn_in_time), '%m-%d-%H:%M')
            due_time = datetime.strftime(exercise_due_date, '%m-%d-%H:%M')
            days_late = (datetime.fromtimestamp(file_turn_in_time) - exercise_due_date).days
            late_periods = int(math.ceil(float(days_late) / sb_data['late_penalty_period']))  # round up half days
            late_penalty = round(sb_data['late_fixed_penalty'] + (late_periods * sb_data['late_penalty_per_period']), 1)
            if late_penalty > sb_data['late_penalty_cap']:  # cap the loss at a fixed amount per exercise
                late_penalty = sb_data['late_penalty_cap']
            # print(days_late, late_periods, late_penalty)
            late_exercises_file.write(
                "{:<20}{:<10}{:<18}{:<13}{:<16}{:<12}{:<10}\n".format(
                    cdt_dir, assignment_file, assignment, due_time, turn_in_time, days_late+1, late_penalty))
            cdt_solves[cdt_dir]['latePoints'] -= late_penalty
            cdt_solves[cdt_dir]['lateList'].append(assignment_file)
            #write_assignment_status(assignment_file, cdt_solves, cdt_dir, exercise_due_date, labs_status_file, late_exercises_file)

    def write_assignment_status(assignment, cdt_solves, cdt_dir, labs_status_file, due_date_datetime):
        due_date = datetime.strftime(due_date_datetime, '%m-%d-%H:%M')
        for assign in [assignment + ".txt", assignment + ".pdf"]:
            if assign in cdt_solves[cdt_dir]['assignments_complete']:
                turn_in_time = cdt_solves[cdt_dir]['assignments_complete_times'][assign]
                turn_in_time = datetime.fromtimestamp(turn_in_time)
                turn_in_time = datetime.strftime(turn_in_time, '%m-%d-%H:%M')
                labs_status_file.write("{:<18}{:<13}{:<13}{:<12}\n".format(
                    assign, due_date, turn_in_time, "Completed"))
            else:
                labs_status_file.write("{:<18}{:<13}{:<13}{:<12}\n".format(
                    assign, due_date, "N/A", "******Missing******"))
        labs_status_file.write("----------------------------------------------\n")

    def split_exercise(exercise):
        """Split a exercise address given as string into a 3 or 4-tuple of integers."""
        return tuple(int(part) for part in exercise.split('.'))

    def exercise_key(item):
        if 'a' in item[0]:
            return split_exercise(item[0][:-1])
        else:
            return split_exercise(item[0])

    def score_first_bloods(cdt_solves):
        first_blood_HTML_string = ''
        tr = '\t\t<tr>\n\t\t\t<td>{exercise}</td>\n\t\t\t<td>{hostname}</td>\n\t\t\t<td>{network}</td>'
        # tr += '\n\t\t\t<td class="blood">{firstBlood}</td>\n\t\t\t<td>{solved}</td>'
        tr += '\n\t\t\t<td class="blood">{firstBlood}</td>'
        tr += '\n\t\t\t<td>{firstBloodTime}</td>\n\t\t\t<td>{lastCompTime}</td>\n\t\t\t<td>{completed}</td>\n'
        for assignment in sorted(assignments_complete.items(), key=exercise_key):
            # put all the completed times in an array
            completed = []
            assignment = assignment[0]
            for time in assignments_complete[assignment]:
                completed.append(nicknames[assignments_complete[assignment][time]])
            completed = set(completed)
            # sort by completed time, pick the first completed to determine first blood
            earliest_date = sorted(assignments_complete[assignment])[0]
            last_date = sorted(assignments_complete[assignment])[-1]
            # add pertinent values to dictionary to use in string format to build html
            assignments_complete[assignment]['exercise'] = hosts[str(assignment)]['IP']
            assignments_complete[assignment]['hostname'] = hosts[str(assignment)]['hostname']
            assignments_complete[assignment]['network'] = hosts[str(assignment)]['network']
            # assignments_complete[assignment]['solved'] = hosts[str(assignment)]['solved']
            timestamp = datetime.fromtimestamp(earliest_date)
            assignments_complete[assignment]['firstBloodTime'] = datetime.strftime(timestamp, '%m-%d at %H:%M')
            # we know the earliestDate so look into the exercisesComplete to find the CDT that submitted it
            cdt_dir = assignments_complete[assignment][earliest_date]
            assignments_complete[assignment]['firstBlood'] = nicknames[cdt_dir]

            if len(completed) == number_of_students:
                assignments_complete[assignment]['completed'] = "<b>All Students</b>"
                timestamp = datetime.fromtimestamp(last_date)
                assignments_complete[assignment]['lastCompTime'] = datetime.strftime(timestamp, '%m-%d at %H:%M')
            else:
                assignments_complete[assignment]['completed'] = ", ".join(sorted(completed))
                assignments_complete[assignment]['lastCompTime'] = '---'
                #timestamp = datetime.fromtimestamp(last_date)
                #assignments_complete[assignment]['lastCompTime'] = datetime.strftime(timestamp, '%m-%d at %H:%M')
            # add points to the cadetSolve for students that get first blood
            time_bin = get_time_bin(earliest_date)
            cdt_solves[cdt_dir]['solves'][time_bin] += sb_data['bonus_points_first_blood']
            cdt_solves[cdt_dir]['totalBonus'] += sb_data['bonus_points_first_blood']
            cdt_solves[cdt_dir]['firstBloods'] += sb_data['bonus_points_first_blood']
            if len(completed) > 1 and sb_data['scoreTwoFirstBloods']:
                second_earliest_date = sorted(assignments_complete[assignment])[1]
                cdt_dir2 = assignments_complete[assignment][second_earliest_date]
                if cdt_dir != cdt_dir2 or (cdt_dir == cdt_dir2 and len(completed) > 2):
                    if cdt_dir == cdt_dir2:
                        second_earliest_date = sorted(assignments_complete[assignment])[2]
                        cdt_dir2 = assignments_complete[assignment][second_earliest_date]
                    cdt_solves[cdt_dir2]['solves'][time_bin] += sb_data['bonus_points_first_blood']
                    cdt_solves[cdt_dir2]['totalBonus'] += sb_data['bonus_points_first_blood']
                    cdt_solves[cdt_dir2]['firstBloods'] += sb_data['bonus_points_first_blood']
                    #recreate first_blood_HTML_string
                    assignments_complete[assignment]['firstBlood'] = nicknames[cdt_dir] + ', ' + nicknames[cdt_dir2]
            first_blood_HTML_string += tr.format(**assignments_complete[assignment])
        return first_blood_HTML_string

    def compute_due_dates_graph_line(cdt_solves):
        cdt_solves['Due Dates'] = dict()
        cdt_solves['Due Dates']['solves'] = [0] * number_of_time_bins
        for exercise, dueDate in assignment_due_dates.iteritems():
            if dueDate < current_date:
                time_bin = get_time_bin(mktime(dueDate.timetuple()))
                cdt_solves['Due Dates']['solves'][time_bin] += sb_data['points_per_assignment'] * sb_data['assignments_per_lab_machine']
        return cdt_solves

    def compute_cdf(solves):
        for cdt_dir in solves:
            solve_str = str(numpy.cumsum(solves[cdt_dir]['solves']))[1:-1]
            solves[cdt_dir]['cdf'] = map(int, [s.strip() for s in solve_str.split(' ') if s])
            # go back through and cap everything at the max_assignment_points
            for index in range(len(solves[cdt_dir]['cdf'])):
                if solves[cdt_dir]['cdf'][index] > sb_data['max_assignment_points']:
                    solves[cdt_dir]['cdf'][index] = sb_data['max_assignment_points']
            solves[cdt_dir]['totalExercisePoints'] = solves[cdt_dir]['cdf'][-1]
            solves[cdt_dir]['percentageComplete'] = \
                [round((float(x) / sb_data['max_assignment_points'])*100, 1) for x in solves[cdt_dir]['cdf']]
        return solves

    def generate_exercises_chart_data(solves_data):
        chart_data_array = []
        for solves_data_name in sorted(nicknames, key=nicknames.get):
            chart_data = dict()
            chart_data['name'] = nicknames[solves_data_name]
            chart_data['data'] = solves_data[solves_data_name]['percentageComplete']
            # Set data to visible by default
            chart_data['visible'] = True
            # Handle Due Dates Line
            if solves_data_name == "Due Dates":
                chart_data['type'] = "area"
                chart_data['color'] = "rgba(255,0,0,1)"
                chart_data['step'] = "left"
                # Save Due Date Data to add last
                due_date_data = chart_data
            else:
                chart_data['type'] = "line"
                for team in teams:
                    if solves_data_name == team['name']:
                        chart_data['color'] = team['color']
                        chart_data['step'] = "left"
                        chart_data['visible'] = False
                        chart_data['dashStyle'] = "longdash"
                    elif "TM" not in solves_data_name:
                        for member in team['members']:
                            # this is because this could be a team or just a CDT
                            if solves_data_name == member:
                                # do not set color so all different
                                # chart_data['color'] = team['color']
                                chart_data['step'] = "left"
                                chart_data['dashStyle'] = "solid"
                chart_data_array.append(chart_data)
        # finally, add the dueDateData at the end so its line is the top line
        chart_data_array.append(due_date_data)
        return chart_data_array

    def generate_points_chart_data(cdt_solves):
        points_array = []
        # {name: 'John', data: [5, 3, 4, 7, 2]}
        extra_assignments_bonus_points = dict()
        extra_assignments_bonus_points['name'] = "Extra Labs Bonus Points"
        extra_assignments_bonus_points['color'] = "blue"
        extra_assignments_bonus_points['data'] = []
        for student in student_names:
            assignments_required = sb_data['required_hosts_compromised'] * sb_data['assignments_per_lab_machine']
            if len(cdt_solves[student]['assignments_complete']) > assignments_required:
                extra_assignments = len(cdt_solves[student]['assignments_complete']) - assignments_required
                extra_assignments = extra_assignments / sb_data['assignments_per_lab_machine']
                extra_assignments_bonus_points['data'].append(extra_assignments * sb_data['bonus_points_per_extra_assignment'])
            else:
                extra_assignments_bonus_points['data'].append(0)
        points_array.append(extra_assignments_bonus_points)
        first_blood_bonus = dict()
        first_blood_bonus['name'] = "First Blood Bonus"
        first_blood_bonus['color'] = "orange"
        first_blood_bonus['data'] = []
        for student in student_names:
            first_blood_bonus['data'].append(cdt_solves[student]['firstBloods'])
        # first_blood_bonus['data'].append(0)
        points_array.append(first_blood_bonus)
        # week_bonus = dict()
        # week_bonus['name'] = "Weekly Team Bonus"
        # week_bonus['color'] = "blue"
        # week_bonus['data'] = []
        # for student in sorted(student_names):
        #     week_bonus['data'].append(cdt_solves[student]['teamBonus'])
        # points_array.append(week_bonus)
        exercise_points = dict()
        exercise_points['name'] = "Exercise Completion Points"
        exercise_points['color'] = "green"
        exercise_points['data'] = []
        for student in student_names:
            if cdt_solves[student]['totalExercisePoints'] > sb_data['max_assignment_points']:
                exercise_points['data'].append(sb_data['max_assignment_points'])
            else:
                exercise_points['data'].append(cdt_solves[student]['totalExercisePoints'])
        # exercise_points['data'].append(cdt_solves['Due Dates']['totalExercisePoints'])
        points_array.append(exercise_points)
        negative_late_points = dict()
        negative_late_points['name'] = "Lost Late Points"
        negative_late_points['color'] = "red"
        negative_late_points['data'] = []
        for student in student_names:
            negative_late_points['data'].append(round(cdt_solves[student]['latePoints'],2))
        # negative_late_points['data'].append(0)
        points_array.append(negative_late_points)
        with open('grades_labs.csv', 'w') as grade_file:
            grade_file.write("Student Name, Execise Points, First Bloods, Late Points\n")
            for student in sorted(student_names):
                grade_string = str(student) + ', '
                grade_string += str(cdt_solves[student]['totalExercisePoints']) + ', '
                #grade_string += str(cdt_solves[student]['teamBonus']) + ', '
                grade_string += str(cdt_solves[student]['firstBloods']) + ', '
                grade_string += str(cdt_solves[student]['latePoints']) + '\n'
                grade_file.write(grade_string)
        return points_array

    def write_chart_data_to_file(chart_data, x_axis_labels, file_name, function_name):
        output_string = function_name + '([\n'
        output_string += json.dumps(x_axis_labels, sort_keys=True, indent=4, separators=(',', ': '))
        output_string += ',\n'
        output_string += json.dumps(chart_data, sort_keys=True, indent=4, separators=(',', ': '))
        output_string += '\n])'
        with open(os.path.join(local_scoreboard_data_folder, 'data', file_name), 'w') as localData:
            localData.write(output_string)

    hosts, assignment_due_dates = get_assignments()
    assignments_complete = dict()
    print("Required assignments: {}, Optional Assignments: {}".format(len(assignment_due_dates), sb_data['number_of_optional_assignments']))
    print("Max total points is " + str(sb_data['max_assignment_points']))
    cadet_solves = get_cadet_solves_data_from_share_folder()
    # cadetSolves = compute__cdf(cadetSolves)
    # recompute the cadet CDF with team points and bonus points
    cadet_solves = compute_due_dates_graph_line(cadet_solves)
    cadet_solves = compute_cdf(cadet_solves)
    # team_solves = compute_team_solves(cadet_solves)
    # team_solves = compute_cdf(team_solves)
    # update cadetSolves with the teamSolves. We do this so team will be visible in the exerciseChart
    # cadet_solves.update(team_solves)
    exercise_chart_data = generate_exercises_chart_data(cadet_solves)
    # calculate the winning team and firstBlood after generate exercises.js so the bonus values are not included in the charts.
    # weekly_HTML, cadet_solves, team_solves, monday_chart_labels = compute_winning_team_weekly(cadet_solves, team_solves)
    # teams_chart_data = generate_teams_chart_data(team_solves)
    first_blood_HTML = score_first_bloods(cadet_solves)
    points_chart_data = generate_points_chart_data(cadet_solves)
    if sb_data['write_to_file']:
        write_chart_data_to_file(exercise_chart_data, x_axis_date_labels, "labs.js", 'create_labs_chart')
        write_chart_data_to_file(points_chart_data, student_nicknames, "labs_points.js", 'create_labs_points_chart')
    print("The number of lab machines is: " + str(len(hosts)))
    return first_blood_HTML

if __name__ == "__main__":
    scoreboard_data = get_default_values()
    nicknames = sb.get_nicknames()
    teams = sb.get_teams()
    build_scoreboard(scoreboard_data, nicknames, teams)
