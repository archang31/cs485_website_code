create_exercises_teams_chart([
[
    "Jan 15",
    "Jan 22",
    "Jan 29",
    "Feb 05",
    "Feb 12",
    "Feb 19",
    "Feb 26",
    "Apr 02",
    "Apr 09"
],
[
    {
        "color": "purple",
        "data": [
            14.97,
            32.65,
            44.22,
            51.02,
            58.5,
            65.31,
            71.43,
            82.99,
            91.84
        ],
        "name": "TM BlueStoneRod",
        "type": "column"
    },
    {
        "color": "green",
        "data": [
            8.16,
            33.33,
            42.86,
            54.42,
            63.27,
            65.31,
            71.43,
            79.59,
            85.71
        ],
        "name": "TM GlazedEffect",
        "type": "column"
    },
    {
        "color": "orange",
        "data": [
            6.8,
            34.01,
            48.3,
            61.9,
            68.71,
            69.39,
            76.19,
            93.88,
            100.0
        ],
        "name": "TM IronAtlas",
        "type": "column"
    },
    {
        "color": "blue",
        "data": [
            7.48,
            34.69,
            44.22,
            57.82,
            65.99,
            70.75,
            75.51,
            87.76,
            100.0
        ],
        "name": "TM NoNames",
        "type": "column"
    }
]
])