#!/usr/bin/env python
from socket import *
import sys

# examples:
# $ ./comp_client.py abcdabcd12341234abcdabcd12341234

# $ cat proof.txt| python comp_client.py

# $ python comp_client.py
# $ abcdabcd12341234abcdabcd12341234

# CHANGE THE BELOW IP ADDRESS
ip_address = '34.196.150.88'
port = 31337
auth_code = '<ENTER THE CODE I SLACKED YOU HERE>'


def client_recv():
    data = sock.recv(1000)
    while data == "":
        data = sock.recv(1000)
    return data

if __name__ == "__main__":
    if len(sys.argv) == 2:
        proof = sys.argv[1]
    else:
        proof = raw_input("[*] Please enter your proof string:\n")
    print('[*] Your proof text is: %s' % proof)
    # Create a TCP/IP socket
    sock = socket(AF_INET, SOCK_STREAM)
    # Connect the socket to the port where the server is listening
    server_address = (ip_address, port)
    print('[*] Connecting to %s port %s.' % server_address)
    sock.connect(server_address)
    print('[*] Sending auth code.')
    sock.send(auth_code)
    print(client_recv()),
    print('[*] Sending proof.')
    sock.send(proof)
    print(client_recv()),





