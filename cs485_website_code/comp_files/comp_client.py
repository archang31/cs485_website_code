#!/usr/bin/env python

from socket import *
import sys

# examples:
# $ ./comp_client.py abcdabcd12341234abcdabcd12341234

# $ cat proof.txt| python comp_client.py

# $ python comp_client.py
# $ abcdabcd12341234abcdabcd12341234

# CHANGE THE BELOW IP ADDRESS
ip_address = '34.196.150.88'
port = 31337
auth_code = 'Y0uRW3bSk1llzWillN0TS@v3Y0u'

users = ['Meow', "Shocktop", "Batman", "Spitfire", "FreeOfKosta", "SwimInSpace", "Admin"]


def get_auth_codes(user):
    auth_codes = dict()
    auth_codes['Meow'] = 'H3r3K1ttyK1ttyC0m32Pl@y'
    auth_codes["Shocktop"] = 'Y0uRW3bSk1llzWillN0TS@v3Y0u'
    auth_codes["Batman"] = 'T1m32Us3Th@tUt1liTyB3Lt'
    auth_codes["Spitfire"] = 'D0Y0uTh1nKY0uAr3aR@pp3r?'
    auth_codes["FreeOfKosta"] = 'M0r3Lik3C@ntGetFr33OfL1m1t3dSh3llz'
    auth_codes["SwimInSpace"] = 'M0r3Lik3Dr0wnDuR1nGPr1VEsc'
    auth_codes["Admin"] = 'ArchAng3l1sTh3B3sT'
    return auth_codes[user]


def client_recv():
    data = sock.recv(1000)
    while data == "":
        data = sock.recv(1000)
    return data

if __name__ == "__main__":
    for i in range(1, len(users)+1):
        print("[" + str(i) + "] " + users[i-1])
    user = users[int(raw_input("Who do you want to be (enter just the number)?\n"))-1]
    machine = raw_input("Which machine number? [Enter three digits like 013 for 013.txt]\n")
    with open("proofs/" + machine + ".txt", 'rt') as f:
        proof = f.readline().strip()
    print('[*] Your proof text is: %s' % proof)
    # Create a TCP/IP socket
    sock = socket(AF_INET, SOCK_STREAM)
    # Connect the socket to the port where the server is listening
    server_address = (ip_address, port)

    print('[*] Connecting to %s port %s.' % server_address)
    sock.connect(server_address)
    print('[*] Sending auth code.')
    sock.send(get_auth_codes(user))
    print(client_recv()),
    print('[*] Sending proof.')
    sock.send(proof)
    print(client_recv()),





