#!/usr/bin/env python

from datetime import datetime
from socket import *
from threading import Thread
import random
import build_scoreboard_comp as cb
from slackclient import SlackClient


def get_auth_codes():
    auth_codes = dict()
    auth_codes['H3r3K1ttyK1ttyC0m32Pl@y'] = "Meow"
    auth_codes['Y0uRW3bSk1llzWillN0TS@v3Y0u'] = "Shocktop"
    auth_codes['T1m32Us3Th@tUt1liTyB3Lt'] = "Batman"
    auth_codes['D0Y0uTh1nKY0uAr3aR@pp3r?'] = "Spitfire"
    auth_codes['M0r3Lik3C@ntGetFr33OfL1m1t3dSh3llz'] = "FreeOfKosta"
    auth_codes['M0r3Lik3Dr0wnDuR1nGPr1VEsc'] = "SwimInSpace"
    auth_codes['ArchAng3l1sTh3B3sT'] = "Admin"
    return auth_codes


def get_slack_connection():
    slack_token = "removed_and_no_longer_valid"
    sc= SlackClient(slack_token)
    return sc


def slack_message(sc, chan, message):
    sc.api_call(
        "chat.postMessage",
        channel=chan,
        text=message,
        as_user=True)


# flavor text for first bloods
def first_flavor(name, box_name):
    flavors = [":fire::fire: " + name + " just got first blood on " + box_name + "! :fire::fire:", ]
    return random.choice(flavors)


# flavor text array for normal solves
def flavor(name, box_name):
    flavors = [name + " just solved " + box_name + "!", ]
    return random.choice(flavors)


def my_print(message):
    print(message)
    with open("server_output.txt", "at") as f:
        time = datetime.strftime(cb.get_time(), '%d@%H:%M:%S ')
        f.write(time + message + '\n')


def update_score(student_name, machine_name, sb_data, fake_time=None, value=None):
    if value:
        student_solves[student_name][machine_name]['earned'] = value
    else:
        student_solves[student_name][machine_name]['earned'] = sb_data['base_points']
        if not assignments[machine_name]['firstBlood']:
            assignments[machine_name]['firstBlood'] = True
            student_solves[student_name][machine_name]['earned'] += sb_data['first_blood_points']
            student_solves[student_name][machine_name]['firstBlood'] = True
    student_solves[student_name][machine_name]['solved'] = True
    if fake_time:
        current_time = datetime.strftime(fake_time, '%m-%d at %H:%M:%S')
    else:
        current_time = datetime.strftime(cb.get_time(), '%m-%d at %H:%M:%S')
    student_solves[student_name][machine_name]['solvedTime'] = current_time
    cb.save_pie_chart_data(student_name, counters[student_name], student_solves, sb_data)
    cb.save_student_data(student_name, student_solves[student_name])
    cb.save_student_data('hosts', assignments)


def handle_client(client_socket, sb_data, sc, chan):
    auth_codes = get_auth_codes()
    ip = client_socket.getsockname()[0] + ":" + str(client_socket.getsockname()[1])
    my_print(ip + ": Received connection.")
    auth_code = client_socket.recv(1000).strip()
    if auth_code not in auth_codes:
        my_print(ip + ": Not valid auth code: " + auth_code)
        client_socket.close()
    else:
        name = auth_codes[auth_code]
        my_print(ip + ": Authenticated as " + name + ".")
        client_msg = "Welcome, " + name + ".\n"
        if name != "Admin":
            client_msg += "Please enter the string from your proof.txt or 'q' to exit.\n"
            client_socket.send(client_msg)
            proof = client_socket.recv(1000).strip()
            if proof == 'q':
                client_msg = ""
                my_print(ip + ": QUIT.")
            elif len(proof) != 32 and len(proof) != 38:
                client_msg = "ERROR: Your input is not the correct length. Check your string and try again.\n"
                my_print(ip + ": ERROR - proof not the correct length.")
            elif proof == 'abcdabcd12341234abcdabcd12341234':
                if student_solves[name]['Bonus']['solved']:
                    client_msg = "You already solved the proof of live. You are not going to get anymore points!\n"
                    my_print(ip + ": ERROR: Sample string sent again.")
                else:
                    client_msg = "Great - I now at least know you are alive. Take 5 points for your effort.\n"
                    update_score(name, 'Bonus', sb_data, None, initial_solve_value)
                    my_print(ip + ": Sample string sent.")
            elif proof not in proofs:
                client_msg = "ERROR: Your proof.txt is not valid. Please check your string.\n" +\
                                 "If you believe your string is correct, please get the instructor.\n"
                my_print(ip + ": ERROR - not a valid proof.txt.")
            else:
                machine_name = proofs[proof]
                box = assignments[machine_name]
                if student_solves[name][machine_name]['solved']:
                    client_msg = "Sorry. You already submitted the proof.txt for " + box['hostname'].title() + ".\n"
                    my_print(ip + ": ERROR - duplicate proof.txt.")
                else:
                    client_msg = "Awesome. You entered the proof.txt for " + box['hostname'].title() + ".\n"
                    my_print(ip + ": Solved " + box['hostname'].title() + ".")
                    if not box['firstBlood']:
                        my_print(ip + ": First Blood on " + box['hostname'].title() + "!")
                        slack_msg = first_flavor(name, box['hostname'].title())
                        client_msg += "Congrats! You scored first blood!.\nYou earned " +\
                                       str(sb_data['base_points'] + sb_data['first_blood_points']) + " points.\n"
                    else:
                        client_msg += "You earned " + str(sb_data['base_points']) + " points.\n"
                        slack_msg = flavor(name, box['hostname'].title())
                    update_score(name, machine_name, sb_data)
                    slack_message(sc, chan, slack_msg)
            client_msg += "Goodbye!\n"
            client_socket.send(client_msg)
    client_socket.close()
    my_print("--------------------------------------------------")


def create_server(sb_data, sc, chan):
    server_socket = socket(AF_INET, SOCK_STREAM)
    server_socket.bind(('0.0.0.0', 31337))
    server_socket.listen(20)
    while True:
        (client_socket, address) = server_socket.accept()
        t = Thread(target=handle_client, args=(client_socket, sb_data, sc, chan))
        t.start()


def do_initial_setup(sb_data):
    cb.generate_initial_student_saves(nicknames, assignments)
    for student in sorted(student_solves):
        cb.save_pie_chart_data(student, counters[student], student_solves, sb_data)


def get_required_baseline_data():
    sb_data = cb.get_default_values()
    nicks = cb.get_nicknames()
    machines, proofs = cb.get_assignments()
    solves = cb.load_all_students_data(nicks)
    counters = cb.get_counters(nicks)
    return sb_data, nicks, machines, proofs, solves, counters


def update_test(sb_data):
    update_score("Meow", '010', sb_data, datetime(2017, 5, 5, 16, 40))
    update_score("Batman", '010', sb_data, datetime(2017, 5, 5, 16, 50))
    update_score("Spitfire", '010', sb_data, datetime(2017, 5, 5, 16, 52))
    update_score("Shocktop", '010', sb_data, datetime(2017, 5, 5, 17, 30))
    update_score("Shocktop", '013', sb_data, datetime(2017, 5, 5, 17, 10))
    update_score("Shocktop", '227', sb_data, datetime(2017, 5, 5, 17, 30))
    update_score("Shocktop", '005', sb_data, datetime(2017, 5, 5, 17, 20))


if __name__ == "__main__":
    initial_solve_value = 5
    scoreboard_data, nicknames, assignments, proofs, student_solves, counters = get_required_baseline_data()
    slack_client = get_slack_connection()
    # channel = "C593E7K8W"  # test channel
    channel = "C573GEHKJ"  # real channel
    my_print("[*] Starting server")
    create_server(scoreboard_data, slack_client, channel)
    # update_test(scoreboard_data)
