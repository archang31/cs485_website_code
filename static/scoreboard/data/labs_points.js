create_labs_points_chart([
[
    "Adambomb",
    "Atlas",
    "BlueberryNinja",
    "Fauer4effect",
    "GlazedDonut",
    "Gowther",
    "IronGiant",
    "Karamazov",
    "Mullenator",
    "R0d",
    "Stonepresto",
    "Vimgod"
],
[
    {
        "color": "blue",
        "data": [
            10,
            40,
            25,
            15,
            0,
            40,
            15,
            15,
            5,
            0,
            5,
            30
        ],
        "name": "Extra Labs Bonus Points"
    },
    {
        "color": "orange",
        "data": [
            2,
            40,
            18,
            8,
            2,
            4,
            4,
            26,
            2,
            4,
            6,
            0
        ],
        "name": "First Blood Bonus"
    },
    {
        "color": "green",
        "data": [
            272,
            272,
            272,
            272,
            216,
            272,
            272,
            272,
            272,
            56,
            272,
            272
        ],
        "name": "Exercise Completion Points"
    },
    {
        "color": "red",
        "data": [
            -36.7,
            -9.0,
            -3.0,
            -13.0,
            -35.5,
            -84.8,
            -51.4,
            0.0,
            -12.0,
            -12.8,
            -19.2,
            -109.4
        ],
        "name": "Lost Late Points"
    }
]
])