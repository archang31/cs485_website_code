create_exercises_points_chart([
[
    "Adambomb",
    "Atlas",
    "BlueberryNinja",
    "Fauer4effect",
    "GlazedDonut",
    "Gowther",
    "IronGiant",
    "Karamazov",
    "Mullenator",
    "R0d",
    "Stonepresto",
    "Vimgod"
],
[
    {
        "color": "orange",
        "data": [
            10,
            62,
            66,
            6,
            20,
            0,
            4,
            22,
            2,
            0,
            4,
            0
        ],
        "name": "First Blood Bonus"
    },
    {
        "color": "blue",
        "data": [
            15,
            30,
            5,
            0,
            0,
            30,
            30,
            15,
            15,
            5,
            5,
            0
        ],
        "name": "Weekly Team Bonus"
    },
    {
        "color": "green",
        "data": [
            269.5,
            269.5,
            269.5,
            269.5,
            231.0,
            269.5,
            269.5,
            269.5,
            269.5,
            236.5,
            264.0,
            203.5
        ],
        "name": "Exercise Completion Points"
    },
    {
        "color": "red",
        "data": [
            -3.5,
            -1.0,
            -0.5,
            -3.0,
            -7.0,
            -3.0,
            -3.0,
            -2.0,
            -15.0,
            -24.0,
            -10.0,
            -10.5
        ],
        "name": "Lost Late Points"
    }
]
])