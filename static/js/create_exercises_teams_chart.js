function create_exercises_teams_chart(data) {
	let xAxisData = data[0];
	let seriesData = data[1];
	console.log(xAxisData);
	console.log(seriesData);
	var ctx = document.getElementById('exercises_teams_chart');
	Highcharts.chart(ctx, {
	    title: {
	        text: 'Team Scores',
	        style: {
	        	fontWeight: 'bold',
	        	fontSize: "20px"
	        },
	    },
	    xAxis: xAxisData,
	    yAxis: {
	        title: {
	            text: 'Team Average Percentage'
	        },
	        plotLines: [{
	            value: 0,
	            width: 1,
	            color: '#808080'
	        }],
	        min: 0,
	        max: 100
	    },
	    xAxis: {
	        categories: xAxisData
	    },
	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'middle',
	        borderWidth: 0
	    },
	    series: seriesData
	});
};