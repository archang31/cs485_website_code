function create_exercises_points_chart(data) {
	let xAxisData = data[0];
	let seriesData = data[1];
	console.log(xAxisData);
	console.log(seriesData);
	var ctx = document.getElementById('exercises_points_chart');
	Highcharts.chart(ctx, {
		chart: {
            type: 'column'
        },
	    title: {
	        text: 'Student Total Points',
	        align: "center",
	        style: {
	        	fontWeight: 'bold',
	        	fontSize: "20px"
	        }
	    },
	    subtitle: {
	        text: 'Final score is sum of positive and negative numbers',
	    },
	    xAxis: {
	        categories: xAxisData
	    },
	    yAxis: {
	        title: {
	            text: 'Student Total Points'
	        },
	        stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold'
                }
            }
	    },
	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'middle',
	        borderWidth: 0
	    },
	    series: seriesData,
	    plotOptions: {
            column: {
                stacking: 'normal',
            }
        },
	});
};