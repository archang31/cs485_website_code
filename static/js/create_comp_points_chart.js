function create_comp_points_chart(data) {
	let studentName = data[0];
	let chartNumber = data[1];
    let seriesData = data[2];
	console.log(studentName);
	console.log(chartNumber);
	console.log(seriesData);
	var ctx = document.getElementById(chartNumber);
	Highcharts.chart(ctx, {
        chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
            type: 'pie',
        },
        title: {
            text: 'Solves for ' + studentName
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.earned:1.f}</b><br>{point.solvedTime}'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                borderColor: '#888888',
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Points',
            data: seriesData,
        }]
    });
};