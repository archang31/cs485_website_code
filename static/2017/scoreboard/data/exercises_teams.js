create_exercises_teams_chart([
[
    "Jan 16",
    "Jan 23",
    "Jan 30",
    "Feb 06",
    "Feb 13",
    "Feb 20",
    "Feb 27"
],
[
    {
        "color": "blue",
        "data": [
            21.74,
            41.3,
            47.83,
            60.87,
            73.91,
            89.13,
            98.91
        ],
        "name": "TM CatMan",
        "type": "column"
    },
    {
        "color": "green",
        "data": [
            23.91,
            40.22,
            45.65,
            65.22,
            73.91,
            85.87,
            98.91
        ],
        "name": "TM FreeOfFire",
        "type": "column"
    },
    {
        "color": "orange",
        "data": [
            4.35,
            29.35,
            36.96,
            57.61,
            75.0,
            91.3,
            95.65
        ],
        "name": "TM SwimTop",
        "type": "column"
    }
]
])