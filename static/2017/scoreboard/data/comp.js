create_comp_chart([
[
    "21:50",
    "21:50",
    "21:51",
    "21:51",
    "21:51",
    "21:52",
    "21:52",
    "21:52",
    "21:53",
    "21:53",
    "21:53",
    "21:54",
    "21:54",
    "21:54",
    "21:55",
    "21:55",
    "21:55",
    "21:56",
    "21:56",
    "21:56",
    "21:57",
    "21:57",
    "21:57",
    "21:58",
    "21:58",
    "21:58",
    "21:59",
    "21:59",
    "21:59",
    "22:00",
    "22:00",
    "22:00",
    "22:01",
    "22:01",
    "22:01",
    "22:02",
    "22:02",
    "22:02",
    "22:03",
    "22:03",
    "22:03",
    "22:04",
    "22:04",
    "22:04",
    "22:05",
    "22:05",
    "22:05",
    "22:06",
    "22:06",
    "22:06",
    "22:07",
    "22:07",
    "22:07",
    "22:08",
    "22:08",
    "22:08",
    "22:09",
    "22:09",
    "22:09",
    "22:10",
    "22:10",
    "22:10",
    "22:11",
    "22:11",
    "22:11",
    "22:12",
    "22:12",
    "22:12",
    "22:13",
    "22:13",
    "22:13",
    "22:14",
    "22:14",
    "22:14",
    "22:15",
    "22:15",
    "22:15",
    "22:16",
    "22:16",
    "22:16",
    "22:17",
    "22:17",
    "22:17",
    "22:18",
    "22:18",
    "22:18",
    "22:19",
    "22:19",
    "22:19",
    "22:20"
],
[
    {
        "data": [
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        ],
        "name": "Batman",
        "type": "line",
        "visible": true
    },
    {
        "data": [
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        ],
        "name": "FreeOfKosta",
        "type": "line",
        "visible": true
    },
    {
        "data": [
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        ],
        "name": "Meow",
        "type": "line",
        "visible": true
    },
    {
        "data": [
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        ],
        "name": "Shocktop",
        "type": "line",
        "visible": true
    },
    {
        "data": [
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        ],
        "name": "Spitfire",
        "type": "line",
        "visible": true
    },
    {
        "data": [
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        ],
        "name": "SwimInSpace",
        "type": "line",
        "visible": true
    }
]
])