create_labs_points_chart([
[
    "Batman",
    "FreeOfKosta",
    "Meow",
    "Shocktop",
    "Spitfire",
    "SwimInSpace"
],
[
    {
        "color": "blue",
        "data": [
            10,
            0,
            0,
            0,
            0,
            0
        ],
        "name": "Extra Labs Bonus Points"
    },
    {
        "color": "orange",
        "data": [
            65,
            5,
            5,
            25,
            20,
            15
        ],
        "name": "First Blood Bonus"
    },
    {
        "color": "green",
        "data": [
            600,
            450,
            390,
            510,
            510,
            540
        ],
        "name": "Exercise Completion Points"
    },
    {
        "color": "red",
        "data": [
            -48.0,
            -84.0,
            -68.0,
            -64.5,
            -130.5,
            -102.0
        ],
        "name": "Lost Late Points"
    }
]
])