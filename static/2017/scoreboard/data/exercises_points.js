create_exercises_points_chart([
[
    "Batman",
    "FreeOfKosta",
    "Meow",
    "Shocktop",
    "Spitfire",
    "SwimInSpace"
],
[
    {
        "color": "orange",
        "data": [
            48,
            40,
            0,
            2,
            0,
            2
        ],
        "name": "First Blood Bonus"
    },
    {
        "color": "blue",
        "data": [
            45,
            45,
            45,
            30,
            45,
            30
        ],
        "name": "Weekly Team Bonus"
    },
    {
        "color": "green",
        "data": [
            920,
            920,
            920,
            920,
            920,
            900
        ],
        "name": "Exercise Completion Points"
    },
    {
        "color": "red",
        "data": [
            -9,
            -3,
            -90,
            -24,
            -84,
            -75
        ],
        "name": "Lost Late Points"
    }
]
])