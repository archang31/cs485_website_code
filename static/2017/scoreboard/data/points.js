createPointsChart([
[
    "KOSTA",
    "LAKES",
    "MAIXNER",
    "MICHELL",
    "SEO",
    "SHOCKLEY"
],
[
    {
        "color": "orange",
        "data": [
            40,
            0,
            48,
            2,
            0,
            2
        ],
        "name": "First Blood Bonus"
    },
    {
        "color": "blue",
        "data": [
            45,
            45,
            45,
            30,
            45,
            30
        ],
        "name": "Weekly Team Bonus"
    },
    {
        "color": "green",
        "data": [
            920,
            920,
            920,
            900,
            920,
            920
        ],
        "name": "Exercise Completion Points"
    },
    {
        "color": "red",
        "data": [
            -3,
            -84,
            -9,
            -75,
            -90,
            -24
        ],
        "name": "Lost Late Points"
    }
]
])