# CS485 Website

[![unlicense](https://img.shields.io/badge/un-license-brightgreen.svg)](http://unlicense.org "The Unlicense") [![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme "RichardLitt/standard-readme")

> Final 2018 CS485 Website and generating code (with minor changes to protect Offensive Security Copyrights).

This repo hosts my course website for CS485 as well as the code used to create and update [this site](https://cs485.mjkranch.com). The code in this repo is an example of combining python with JavaScript to create a dynamic feeling website with only static components (and then using [Hugo](https://gohugo.io/) to serve the site).
[The exercise scoreboard](https://cs485.mjkranch.com/scoreboard_exercises.html) ([code](../static/scoreboard_exercises.html)) and [the live competition scoreboard](https://cs485.mjkranch.com/2017/scoreboard_comp.html) ([code](../static/2017/scoreboard_comp.html)) are the best two examples of the final product.

### Quick Warning

I have been publicly serving the course website for a while so my students could continue to reference it, but I just recently made the code behind the website available as another example of one of my coding projects. I did not intend for this project to be publicly released so the documentation is limited and some of the functionality is incomplete. If you notice anything that might be copyrighted material belonging to Offensive Security, please contact me immediately at [michael@mjkranch.com](mailto:michael@mjkranch.com).

## Table of Contents

- [Introduction](#introduction)
- [Directory Structure Explained](#directory)
- [Code Parts Breakdown](#code-breakdown)
    - [Generating the base site static pages](#generating-the-base-site-static-pages-lessonshtml-resourceshtml-etc)
    - [Generating the scoreboard pages](#generating-the-scoreboard-pages)
    - [Pushing the updates to the servers](#generating-the-scoreboard-pages)
    - [The Live Competition](#the-live-competition)
- [Contribute](#contribute)
- [License](#license)

## Introduction
This code started as a method of auto-updating my course website from my lesson schedule spreadsheet. Essentially, I simply wanted to maintain a single excel file and have the website always reflect any changes to homework, reading, etc. without manually updating the static html or push files and attachments to the server (essentially, create my own static site generator using a custom format). Later, I wanted to include an auto-updating scoreboard to motivate my students, but I had the significant limitation of not being able to use any dynamic code (the department servers where only able to serve static html, css, and js).  Long story short, I solved this problem by separating the functionality into pieces which I will discuss in more detail below. I use a python script to update the static html from my Microsoft Excel file and generate a JSON object. I also output a .csv copy of the lesson file since the web server is Linux (and can not process a .xlsx file). I then leverage JavaScript via [Highcharts](https://www.highcharts.com/) to ingest this JSON to create the scoreboard / charts. Finally, I use the python script to copy all these files to the web server, and the server runs a limited version of my python script every 5 minutes to update the website. This all has the feel of live updating data, but it is really all static files periodically updated.

## Directory

 * [cs485_website_code/](./cs485_website_code) - the code that generates the static files. [More detail here](#dynamically-updating-the-static-files-with-build_complete_websitepy)
     * [base_pages/](./cs485_website_code/base_pages) - these are base page templates used by [build_complete_website.py](./cs485_website_code/build_complete_website.py) to create the site html
         * [base_home.html](./cs485_website_code/base_pages/base_home.html) - this is an example of a simple page with content (this is index.html)
         * [base_lessons.html](./cs485_website_code/base_pages/base_lessons.html) - this is the more complicated lesson.html
         * [base_lessons_table.txt](./cs485_website_code/base_pages/base_lessons_table.txt) - this is an example of one of the two templates for a lesson table row
         * [base_page.html](./cs485_website_code/base_pages/base_page.html) - this is the standardized header/footer (like in a normal static site generator)
     * [build_complete_website.py](./cs485_website_code/build_complete_website.py) - this is the base (glue) script that generates the static html. See
     * [build_scoreboard.py](./cs485_website_code/build_scoreboard.py) - glue script for exercises. Calls comp/exercises/labs.py
     * [build_scoreboard_comp.py](./cs485_website_code/build_scoreboard_comp.py) - sub script that generates the competition page - [example](https://cs485.mjkranch.com/2017/scoreboard_comp.html)
     * [build_scoreboard_exercises.py](./cs485_website_code/build_scoreboard_exercises.py) - sub script that generates the exercise page - [example](https://cs485.mjkranch.com/2017/scoreboard_exercises.html)
     * [build_scoreboard_labs.py](./cs485_website_code/build_scoreboard_labs.py) - sub script that generates the labs page - [example](https://cs485.mjkranch.com/2017/scoreboard_labs.html)
     * [comp_files/](./cs485_website_code/comp_files) - support files for the live competition. [More detail here](#the-live-competition)
         * [comp_client.py](./cs485_website_code/comp_files/comp_client.py) - This is the base client I gave to the students for the comp.
         * [comp_client_example.py](./cs485_website_code/comp_files/comp_client_example.py) - This is my client to test being any student.
         * [comp_server.py](./cs485_website_code/comp_files/comp_server.py) - This is the live scoring server.
     * [data/](./cs485_website_code/data) - This is data generated by the build_scoreboard_*.py files above that is invested to generate the various charts. These are really simple JSON files.
         * [exercises.js](./cs485_website_code/data/exercises.js)
         * [exercises_points.js](./cs485_website_code/data/exercises_points.js)
         * [exercises_teams.js](./cs485_website_code/data/exercises_teams.js)
     * [js/](./cs485_website_code/js) - These are the JavaScript files that ingest the /data files to actually generate the charts.
         * [create_comp_chart.js](./cs485_website_code/js/create_comp_chart.js)
         * [create_comp_points_chart.js](./cs485_website_code/js/create_comp_points_chart.js)
         * [create_exercises_chart.js](./cs485_website_code/js/create_exercises_chart.js)
         * [create_exercises_points_chart.js](./cs485_website_code/js/create_exercises_points_chart.js)
         * [create_exercises_teams_chart.js](./cs485_website_code/js/create_exercises_teams_chart.js)
         * [reload.js](./cs485_website_code/js/reload.js)
     * [lessons.xlsx](./cs485_website_code/lessons.xlsx) - this is my base schedule (excel) that is changed often
     * [lessons.csv](./cs485_website_code/lessons.csv) - this is the simple .csv version that can be used by the Linux server.
 * [static/](./static) - the final static website files. The base site is the 2018 CS485 version
     * [static/2017/](./static/2017) - This is the 2017 CS485 version
 * [docs/](./docs) - Gitlab documentation and COC stuff.
 * [content/](./content) - not important HUGO stuff
 * [layouts/](./layouts) - more not important HUGO stuff
 * [config.toml](./config.toml) - The Hugo config file
 * [.gitlab-ci.yml](./.gitlab-ci.yml) - Gitlab config file so it knows this is a Hugo repo

## Code Breakdown

### Dynamically updating the static files with [build_complete_website.py](../cs485_website_code/build_complete_website.py)
This is the main file controlling the entire process and serves three purposes. First, this code updates the base html pages with the most complicated being lessons.html. Secondly, it generates the various scoreboards including the JSON objects. Finally, this code copies all the updated files to the web-server.

### Generating the base site static pages (lessons.html, resources.html, etc.)
[Build_complete_website.py](../cs485_website_code/build_complete_website.py) starts by updating all the static pages with individual calls to the `build_page()` function. The most interesting of these is `build_lessons()`. This function really does two things. First, it loads the predefined lessons table template [base_lessons_table.txt](../cs485_website_code/base_pages/base_lessons_table.txt) that has several named placeholders like `{Date}`. The function then reads the [lessons.xls](../cs485_website_code/lessons.xlsx) and then parses this excel file and using my own markdown-like code format to generate things like `<a>` tags, highlights, breaks, etc. This parsed data results in a dictionary in the courseData dictionary, `courseData['lessons_table']` . It then calls `build_lessons()`. This function then uses the corresponding `base_xxx.html` like `base_lessons.html` based on the name input parameter. Similar to `build_lessons()`, this function uses named placeholders and the courseData dictionary to fill in data and generate the static html page, [lessons.html](./static/lessons.html). Some functions:

* `set_current_tab(page)` - highlights the current tab
* `turnIntoList(rowArrayFromExcel, bulletize=True)` - custom "markdown" like feature for creating html bullets (`<ul>`) from lessons.xlsx.
* `checkIfLinkMultipleItems(rowArrayFromExcel, target="_blank")` - custom "markdown" like feature for adding `<br>` from lessons.xlsx.
* `checkIfLink(item, target="_blank")` - custom "markdown" like feature for adding `<a>` from lessons.xlsx.


### Generating the scoreboard pages

Now that we have created the main pages, I need to create the "live" scoreboard pages. Since the server can only serve static files, I use JavaScript [like this](../cs485_website_code/js/create_comp_points_chart.js) to build the scoreboard charts and graphs based a simple function call with a JSON object [like this](../cs485_website_code/data/labs.js) and just update this JSON object periodically. Finally, I use a python script to iterate through the student's turn-in folders, dynamically generate this JSON, and then push these updated files to the server.

All this updating is done by a call to [build_scoreboard.py](./cs485_website_code/build_scoreboard.py). This file defines some base student data (like the relationship between the named student turn-in folder and the scoreboard handles) and then calls the individual scoreboard scripts ([build_scoreboard_exercises.py](./cs485_website_code/build_scoreboard_exercises.py) for the exercises scoreboard, [build_scoreboard_labs.py](./cs485_website_code/build_scoreboard_labs.py) for the labs, and [build_scoreboard_comp.py](./cs485_website_code/build_scoreboard_comp.py) for the live competition page). Ultimately, these sub-scripts do three main things: they determine the students' score, they generate/update the pages' static html, and they create the JSON that leverages [Highcharts](https://www.highcharts.com/) to create the graphs. I will use exercises as the main example and further discuss my live-competition scoreboard page down below.

1.  Determining the student's score: Essentially, I simply update the scoreboard based on whether or not the student has submitted a document in the correct format into the student's individual turn-in folder, and I use the turn-in timestamp to see if it was late. I have to manually check these submissions at a later point in time to see if they are correct, and I just delete them if its incorrect (its 100% or nothing and timestamp determines points). In [build_scoreboard_exercises.py](./cs485_website_code/build_scoreboard_exercises.py),  `build_scoreboard(sb_data, nicknames, teams)` is then the main function that ultimately outputs both the updated html and the JSON. The default values `(sb_data, nicknames, teams)` are passed in from the call to this function within [build_scoreboard.py](./cs485_website_code/build_scoreboard.py) - the function `get_default_values()` is a old and only used in testing. `sb_data` is the base dictionary of pre-defined data (like score per points, start time, etc.).This function first reads `lessons.csv` to determine what assignment are due as well as the due date for these assignments. Here are the general purpose of the sub-functions:

    a. `get_cadet_solves_data_from_share_folder()` - creates the dictionary `cdt_solves` from reading the student shares to determine what assignments submitted and if they were late.

    b. `score_first_bloods(cdt_solves)` - uses `cdt_solves` to determine the first person (or two) to solve each exercise. Outputs `first_blood_HTML_string` which is what goes into the `{exercises_first_blood_table}` placeholder in [base_scoreboard_exercises.html](./cs485_website_code/base_pages/base_scoreboard_exercises.html). Also updates `cdt_solves` point total.

    c. `compute_team_solves(cdt_solves)` - creates `tm_solves` that was used to generate the team_points chart.

    d. `compute_winning_team_weekly(cdt_solves, tm_solves)` - similar to `score_first_bloods`, this uses `tm_sovles` to create `weekly_HTML_string` which is what goes into the `{weekly_table}` placeholder in [base_scoreboard_exercises.html](./cs485_website_code/base_pages/base_scoreboard_exercises.html).

2. Generate/update the pages' static html: As already discussed, these functions create the static html for `Weekly Winners` and `First Bloods`. If you look at the default base_page [base_scoreboard_exercises.html](./cs485_website_code/base_pages/base_scoreboard_exercises.html), this file is looking for two placeholders, `{weekly_table}` and `{exercises_first_blood_table}`.

    a. `score_first_bloods(cdt_solves)` - uses `cdt_solves` to determine the first person (or two) to solve each exercise. Outputs `first_blood_HTML_string` which is what goes into the `{exercises_first_blood_table}` placeholder in [base_scoreboard_exercises.html](./cs485_website_code/base_pages/base_scoreboard_exercises.html). Also updates `cdt_solves` point total.

    b. `compute_winning_team_weekly(cdt_solves, tm_solves)` - similar to `score_first_bloods`, this uses `tm_sovles` to create `weekly_HTML_string` which is what goes into the `{weekly_table}` placeholder in [base_scoreboard_exercises.html](./cs485_website_code/base_pages/base_scoreboard_exercises.html).

3.  Create the JSON .js file that leverages [Highcharts](https://www.highcharts.com/) to create the graphs: Finally, we need to create these beautiful graphs like `Exercise Completion Percentage`, `Team Scores`, and `Student Total Points` in [scoreboard_exercises.html](./static/scoreboard_exercises.html). This is done in two parts. First, I create a define a static JavaScript function that is included in [base_scoreboard_exercises.html](./cs485_website_code/base_pages/base_scoreboard_exercises.html) which defines what to do when called with some chart data. For example, [create_exercises_chart.js](./cs485_website_code/js/create_exercises_chart.js) is simply a function `create_exercises_chart(data)` that is expecting some `data` that is an array which two elements, the `xAxisData` and the `seriesData` (y-axis data). This function defines what html element to update, how to label the graph, the type of graph, etc. I also include the data (the file that is periodically updated) in [base_scoreboard_exercises.html](./cs485_website_code/base_pages/base_scoreboard_exercises.html) like [exercises.js](./cs485_website_code/data/exercises.js) . This file is simply a call to the previously defined `create_exercises_chart(data)` function that includes the (periodically updated) data. Finally, I use [reload.js](./cs485_website_code/js/reload.js) to reload the site to refresh the data and [try to] mitigate browser caching (it mostly works). Important functions:

    a. `compute_cdf(solves)` - this uses `numpy` to create the cdf that is the main feature in the [Exercise Completion Percentage Chart](./static/scoreboard_exercises.html).

    b. `generate_exercises_chart_data(solves_data)` - exactly what it says! This sets the color, style, labels, etc. based on values set in `nicknames` and outputs a `chart_data_array` used to create the `Exercise Completion Percentage chart` via [exercises.js](./cs485_website_code/data/exercises.js).

    c. `generate_teams_chart_data(solves_data)` - Same as above but for the `Team Scores` via [exercises_teams.js](./cs485_website_code/data/exercises_teams.js).

    d. `generate_points_chart_data(cdt_solves)` - Same as above but for the `Student Total Points` via [exercises_points.js](./cs485_website_code/data/exercises_points.js).

    b. `write_chart_data_to_file()` - this function takes the input data and outputs it as a JSON for the charts (same format for the line and bar graphs). It creates the three above .js files.

### Pushing the updates to the servers

Finally, I have to push all these updates to the server. I use the various `copy_xxxxxx()` functions within [build_complete_website.py](../cs485_website_code/build_complete_website.py) for this task. They use `shutil` and `copytree` to copy data and create backups. These are limited by time (configurable with `write_only_past_week`) to save time by only copying what was recently updated as well as with `write_local` and `write_network` to either do only local changes (for testing) or to publish.

### [The Live Competition](https://cs485.mjkranch.com/2017/scoreboard_comp.html)

Finally, I hosted a [live competition](https://cs485.mjkranch.com/2017/scoreboard_comp.html) during my first CS485 semester. The students had to resolve as many of the PWK boxes as possible within a 3 hour period. I had referees that would verify they had a live shell on the box, and the students would then use [this simple client - comp_client_example.py](./cs485_website_code/comp_files/comp_client_example.py) to submit the proof to my scoring server. [This server - comp_server.py](./cs485_website_code/comp_files/comp_server.py) authenticated the student, verified the proof, and updated a pie chart per student with their score. Below are some of the new files for this competition, the most significant of which was the server w/ Slack.

* [comp_server.py](./cs485_website_code/comp_files/comp_server.py) - This is the file that handled the inbound proofs from the students and updated the scoring logs
    * `create_server(sb_data, sc, chan)` - the threaded server
    * `handle_client(client_socket, sb_data, sc, chan)` - Handled the incoming request and determined if it was valid. If so, it called `update_score()` and `slack_message()`.
    * `update_score(name, machine_name, sb_data)` - This updated the logs file used to generate the pie-chart data.
    * `slack_message(sc, chan, slack_msg)` - Generated Slack messages announcing student scoring.
* [comp_client_example.py](./cs485_website_code/comp_files/comp_client_example.py) - The client provided to the students.
* [comp_client.py](./cs485_website_code/comp_files/comp_client.py) - My client for testing.
* [build_scoreboard_comp.py](./cs485_website_code/build_scoreboard_comp.py) - The script that used the generated logs to update the website.
* [create_comp_points_chart.js](./cs485_website_code/js/create_comp_points_chart.js) - The function used to generate the pie-charts.
* [comp_student1.js](./static/2017/scoreboard/data/comp_student1.js) - An example of the call to `create_comp_chart(data)` with data to generate one pie-chart.

## Contribute

> Contributors to this project are expected to adhere to our [Code of Conduct](docs/CODE_OF_CONDUCT.md "Code of Conduct").

I welcome [issues](docs/issue_template.md "Issue template"), but I prefer [pull requests](docs/pull_request_template.md "Pull request template")! See the [contribution guidelines](contributing.md "Contributing") for more information.

## License

This code is [set free](LICENSE).
